
#ECard
    O propósito do Ecard é possibilitar a criação objetos gráficos para compartilhar informações de maneira persistente, fácil e segura.
    Para o usuário montar um objeto gráfico ele cria sessões e campos, os campos são as informações do objeto que podem ser do tipo
    Geografico, Texto, Numero, Data, etç. As sessões são formas para organizar as informações dos objetos hierarquicamente, por exemplo
    um objeto pode ter várias sessões cada sessão podem ter várias sessões filhas e campos filhos.

    A ideia e que o usuário possa organizar um objeto grafico apenas arrastanto campos e sessões e determinando a posição x e y que o campo deve ser mostrado
    dentro da sessão, a orientação das sessões filhas de uma sessão pai, horizontal ou vertical e a possição da sessão filha entre as outras sessões dentro da
    sessão pai.

    Um objeto ao ser criado pode ser criado dentro de determinados moldes que são Cartão de visitas, menu / catalogo, Etiqueta. A vantagem de criar dentro de um moldes
    e que ao gerar o objeto dentro do molde ele ja é iniciado com os campos necessários criados, e que podem ser achados em pesquisas por objetos especificos, porém ao serem salvos
    é realizada uma validação para que esse objeto esteja dentro dos padões do molde.

    Cada sessão e cartão pode conter filtros para que só possam ser vistar por pessoas que falem determinada linguagem ou estejam em determinada região, enquanto
    cada campo pode ter filtros para determinar que seu valor esteja dentro de algum padrão.

    Cada pessoa pode guardar os cartões de outras e organiza-los dentro de uma estrutura de pastar, para facilitar a organização pessoal.
    Cartões podem ser pesquisados por proximidade, por valores de campos, tipos de objetos.

    Uma pessoa pode seguir outra pessoa ou empresa, para ficar por dentro das atividades e criações de novos objetos do mesmo.
    Uma pessoa pode bloquear um cartão para determinados tipos de pesquisa, torando acessivel apenas atraves do codigo do cartão diretamente, assim como também
    podem tornar o cartão invisivel para tudo, uma pessoa ou empresa também podem se tornar invisivel para não serem seguidos.

## Refatoração
  - *Terminar Icon Buttons e acções em fields
  - Separar o componente Card em  CardEdit, CardShow e CardExport
  - Verificar possibilidade de todos os componentes de Field herdar o componente field e mudar só o metodo render
  - Instalar Redux e Saga
  - Isolar campos de states no componente Field responsáveis pela rederização dinamica
  - Separar os componentes do tipo Field em Edit e Show
  - Melhorar apresentação dos Icon buttons no Card e no Field (Posição fixa, mudar bkg ao aparecer)
  - Melhorar a disposição dos Fields em Grid, inserer mais opções de metadado
  - Inserir opção de posicionamento do coteudo dentro do field no metadado
  - Inserir tela de cadastro de metadados
  - Inserir copy to clipboard nos campos
  - Telas Reset Password *Publico , Change Password dentro do Profile
  - Visual (Icone, Imagens, visual da tela, bkg)
  - Criar chaves para acoes em tela, e uma variavel para identificar qual é a operação atual ex: Card(EXPORT, EDIT, SHOW, METADATA)  Field(EDIT, SHOW, METADATA)
  - Verificar o uso se contentEditable no lugar de input para fields

## Global NPM Modules
  - react-scripts `npm i -g react-scripts`     


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

