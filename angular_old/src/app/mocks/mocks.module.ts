import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { CoreModule } from './core/core.module';
import { environment } from '../../environments/environment';

@NgModule({
  imports: [
    CoreModule
  ]
})
export class MocksModule {
  static mockEnvName = 'MOCK';

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MocksModule,
      providers: []
    };
  }

  static forMocksEnv(): Array<ModuleWithProviders> {
    if (environment.envName === this.mockEnvName) {
      const returnValue: Array<ModuleWithProviders> = new Array<ModuleWithProviders>(
        this.forRoot()
      );
      return returnValue;
    } else {
      return new Array<ModuleWithProviders>();
    }
  }

}
