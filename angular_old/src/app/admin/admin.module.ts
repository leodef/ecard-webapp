import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { SharedModule as AppSharedModule} from '../shared/shared.module';
import { routing } from './admin.routing';

@NgModule({
  imports: [
    SharedModule,
    AppSharedModule,
    CoreModule.forRoot(),
    routing
  ],
  declarations: [
  AdminComponent
  ]
})
export class AdminModule { }
