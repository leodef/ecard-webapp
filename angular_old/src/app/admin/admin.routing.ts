import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import {AdminComponent} from './admin.component';

const routes: Routes = [
  { path: '',
    component: AdminComponent,
    data: [{admin_root: true}],
    pathMatch: 'full' },
  /*
  { path: 'account',
    component: AdminComponent,
    data: [{admin_root: false}],
    loadChildren: './account/admin-account.module#AdminAccountModule'
  },
  */
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
