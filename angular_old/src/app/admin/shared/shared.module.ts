import { NgModule } from '@angular/core';
import { SharedModule as AppSharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    AppSharedModule
  ],
  exports: [
    AppSharedModule,
  ],
  declarations: []
})
export class SharedModule { }
