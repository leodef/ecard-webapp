export class AccountDTO {
    _id: string;
    name: string;
    email: string;
    accountType: string;
    gender: string;
    birth: string;
}
