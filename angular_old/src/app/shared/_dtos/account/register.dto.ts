export class RegisterDTO {
  name: string;
  email: string;
  username: string;
  password: string;
  gender: string;
  birth: string;
  confirmAccountURL: string;
}
