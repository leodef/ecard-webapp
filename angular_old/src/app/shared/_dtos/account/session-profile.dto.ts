import { AccountDTO } from './account.dto';

export class SessionProfileDTO {
    account: AccountDTO;
}

