
export class RowCardDTO {
  _id: string;
  title: string;
  language: string;
  color: string;
  traking: boolean;
  type: string;
  code: string;
}
