import { SessionProfileDTO } from './account/session-profile.dto';

export class PersonSessionProfileDTO extends SessionProfileDTO {
    id: number;
    nickname: string;
    imageURL: string;
    reachable: boolean;
}
