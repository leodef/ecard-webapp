import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgxQRCodeModule } from 'ngx-qrcode3';
import { ImageInputComponent } from './image-input/image-input.component';
import { UniqueInputComponent } from './unique-input/unique-input.component';
import { SelectOptionComponent } from './select-option/select-option.component';
import { ColorDemoComponent } from './color-demo/color-demo.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { MessageComponent } from './message/message.component';
import { DateInputComponent } from './date-input/date-input.component';
import { ColorInputComponent } from './color-input/color-input.component';
import { ButtonsModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { OptionService } from './_services/option.service';
import { InputErrorsComponent } from './input-errors/input-errors.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ButtonsModule,
    BsDatepickerModule,
    NgxQRCodeModule
  ],
  declarations: [
    ImageInputComponent,
    UniqueInputComponent,
    SelectOptionComponent,
    ColorDemoComponent,
    ProgressBarComponent,
    MessageComponent,
    DateInputComponent,
    ColorInputComponent,
    InputErrorsComponent
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ButtonsModule,
    BsDatepickerModule,
    NgxQRCodeModule,
    ImageInputComponent,
    UniqueInputComponent,
    SelectOptionComponent,
    ColorDemoComponent,
    ProgressBarComponent,
    MessageComponent,
    DateInputComponent,
    ColorInputComponent,
    InputErrorsComponent
  ],
  providers: [
    OptionService
  ]
})
export class SharedModule { }
