import { Component, OnInit, Input, Output, Optional, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { BsDatepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { listLocales, defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import * as moment from 'moment';
import { Validators, FormGroup, FormBuilder, AbstractControl, FormControl } from '@angular/forms';

@Component({
  selector: 'app-date-input',
  templateUrl: './date-input.component.html',
  styleUrls: [
    './date-input.component.css']
})
export class DateInputComponent implements OnInit, OnChanges {

  @Input() format = 'DD/MM/YYYY';

  @Input() locale = 'pt-br';

  @Input() date: string = null;

  @Output() dateChange: EventEmitter<string> = new EventEmitter<string>();

  @Output() control: EventEmitter<AbstractControl> = new EventEmitter<AbstractControl>();

  _form: FormGroup;

  bsValue: Date = new Date();

  locales = listLocales();



  constructor(
    private _localeService: BsLocaleService,
    private _datepickerConfig: BsDatepickerConfig,
    private formBuilder: FormBuilder) {
  }

  ngOnInit() {

    defineLocale(this.locale, ptBrLocale);
    this._localeService.use(this.locale);
    this._datepickerConfig.dateInputFormat = this.format;
    this.date = this.date || moment().format(this.format).toString();
    this.bsValue = moment(this.date, this.format).toDate();

    this._form = this.formBuilder.group({
      date: [
        this.bsValue,
        [
          Validators.required
        ], [
          this.validateDate.bind(this)
        ]
      ]
    });
    this._form.valueChanges.subscribe(data => {
      this.changeDate(data.date);
    });

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.date) {
      this.bsValue = moment(changes.date.currentValue, this.format).toDate();
    }
    if (changes.locale) {
      this._localeService.use(changes.locale.currentValue);
    }
    if (changes.format) {
      this._datepickerConfig.dateInputFormat = changes.format.currentValue;
    }
  }


  changeDate(date: Date) {
    if (date && !isNaN(date.getTime())) {
      this.date = moment(date).format(this.format);
      this.dateChange.emit(this.date);
      if (this._form) {
        this.control.emit(this._form.get('date'));
      }
    }
    return false;
  }

  validateDate(control: AbstractControl) {
    // cotrol.value//##
    return Promise.resolve(null);
  }

}
