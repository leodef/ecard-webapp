import { Component, OnInit, Input, Optional } from '@angular/core';
import { FormControl, AbstractControlDirective, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-input-errors',
  templateUrl: './input-errors.component.html',
  styleUrls: ['./input-errors.component.css']
})
export class InputErrorsComponent implements OnInit {

  @Input() control: any;

  
  @Input() @Optional() presenter: Presenter;
  
  constructor() { }

  ngOnInit() {
  }

  showErrors():boolean {
    if(!this.control){return false;}
    return this.control.invalid && this.control.touched;
  }

  getErrors(){
    if(this.control){
      let errors = new Array();
      for(let error in this.control.errors){      
          errors.push(error);
      }
      return errors;
    }
    return [];
  }
}

export class Presenter{
  public show(key: String): String{
    return key;
  }
}