import { SectionLocation } from './section-location';

export class Card {
  _id: string;
  title: string;
  language: string;
  color: string;
  traking: boolean;
  type: string;
  code: string;
  sectionLocationList: SectionLocation[];
}
