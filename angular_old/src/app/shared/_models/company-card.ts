import { Company } from './company';
import { Card } from './card';

export class CompanyCard {
    _id: string;
    company: Company;
    card: Card;
}
