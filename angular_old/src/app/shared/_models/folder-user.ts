import { User } from './user';
import { Folder } from './folder';

export class FolderUser {
    _id: string;
    user: User;
    folder: Folder;
}
