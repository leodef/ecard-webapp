import { FieldValidator } from './field-validator';

export class Field {
    _id: string;
    name: string;
    type: string;
    fieldValidatorList: Field;
}
