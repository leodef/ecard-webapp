import { Field } from './field';

export class SectionField {
    _id: string;
    column: number;
    row: number;
    value: string;
    title: string;
    field: Field;
}
