import { User } from './user';

export class UserToken {
    _id: string;
    expiryDate: Date;
    type: string;
    user: User;
}
