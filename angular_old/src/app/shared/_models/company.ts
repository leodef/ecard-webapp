import { CompanyUser } from './company-user';
import { CompanyCard } from './company-card';

export class Company {
    _id: string;
    name: string;
    code: string;
    type: string;
    codeType: string;
    companyUserList: CompanyUser[];
    companyCardList: CompanyCard[];
}
