import { Company } from './company';
import { User } from './user';

export class CompanyUser {
    _id: string;
    company: Company;
    user: User;
}
