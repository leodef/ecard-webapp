import { SectionField } from './section-field';
import { SectionLocation } from './section-location';

export class Section {
  _id: string;
  title: string;
  color: string;
  // orientation: String;
  columnN: number;
  rowN: number;
  sectionFieldList: SectionField[];
  sectionLocationList: SectionLocation[];
}
