import { Section } from './section';

export class SectionLocation {
   _id: string;
   position: number;
   section: Section;

   static compare(a, b): number {
      const aSectionLocation: SectionLocation = a as SectionLocation;
      const bSectionLocation: SectionLocation = a as SectionLocation;
      if (aSectionLocation.position < bSectionLocation.position) {
         return -1;
      } else if (aSectionLocation.position > bSectionLocation.position) {
         return 1;
      }
      return 0;
   }

}

