import { Folder } from './folder';
import { User } from './user';

export class Contact {
  _id: string;
  contactId: string;
  contactType: string;
  folder: Folder;
  user: User;
}
