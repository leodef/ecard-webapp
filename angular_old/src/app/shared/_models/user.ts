import { Account } from './account';
import { UserCard } from './user-card';
import { CompanyUser } from './company-user';

export class User {
    _id: string;
    nickname: string;
    account: Account;
    userCardList: UserCard[];
    companyUserList: CompanyUser[];
}
