import { UserToken } from './user-token';
import { AccountGrantedAuthority } from './account-granted-authority';

export class Account {
    _id: string;
    name: string ;
    username: string;
    email: string ;
    password: string ;
    accountType: string ;
    gender: string ;
    birth: Date;
    profileImageURL: string;
    grantedAuthority: AccountGrantedAuthority;
    enabled: boolean;
    userTokenList: UserToken[];
}
