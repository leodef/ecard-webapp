import { Card } from './card';
import { User } from './user';

export class UserCard {
    _id: string;
    user: User;
    card: Card;
}
