import { Contact } from './contact';

export class Folder {
    _id: string;
    title: string;
    isRoot: boolean;
    parent: Folder;
    contactList: Contact[];
    folderList: Folder[];
}

