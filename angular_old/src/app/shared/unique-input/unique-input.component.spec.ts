import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniqueInputComponent } from './unique-input.component';

describe('UniqueInputComponent', () => {
  let component: UniqueInputComponent;
  let fixture: ComponentFixture<UniqueInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniqueInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniqueInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
