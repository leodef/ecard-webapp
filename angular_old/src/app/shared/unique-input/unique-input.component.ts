import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-unique-input',
  templateUrl: './unique-input.component.html',
  styleUrls: ['./unique-input.component.css']
})
export class UniqueInputComponent implements OnInit {


  @Input()
  value: string;


  @Output() valueChange:EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

}
