const GENDER = {
  M: 'M',
  F: 'F'
};

export default GENDER;
