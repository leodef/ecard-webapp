const CONTACT_TYPE = {
  CARD: 'CARD',
  USER: 'USER',
  COMPANY: 'COMPANY'
};

export default CONTACT_TYPE;
