import { Injectable } from '@angular/core';

@Injectable()
export class OptionService {

  constructor() { }

  getOptionsFor(inputOptions: any, translate: Function): Array<Option> {

    if (inputOptions instanceof Array) {
      return this.getOptionsForArrayOrSet(inputOptions, translate);

    } else if (inputOptions instanceof Map) {
      return this.getOptionsForMap(inputOptions, translate);

    } else {
      return this.getOptionsForObject(inputOptions, translate);

    }
  }

  getOptionsForObject(inputOptions: Object, translate: Function): Array<Option> {
    const options: Array<Option> = new Array<Option>();
    for (const property in inputOptions) {
      if (inputOptions.hasOwnProperty(property)) {
        const option = inputOptions[property];
        options.push(this.newOptionForStringOrObject(option, translate));
      }
    }

    return options;
  }

  getOptionsForMap(inputOptions: Map<string, string>, translate: Function): Array<Option> {
    const options: Array<Option> = new Array<Option>();

    inputOptions.forEach((value: string, key: string) => {
      options.push(this.newOption(key, value, translate));
    });

    return options;
  }

  getOptionsForArrayOrSet(inputOptions: Array<any>, translate: Function): Array<Option> {
    const options: Array<Option> = new Array<Option>();

    for (const option of inputOptions) {
      options.push(this.newOptionForStringOrObject(option, translate));
    }
    return options;
  }

  // newOption
  newOptionForStringOrObject(option: any, translate: Function): Option {
    if (option instanceof String) {
      return this.newOptionForString(option.toString(), translate);
    } else {
      return this.newOptionForObject(option, translate);
    }
  }

  newOptionForString(option: string, translate: Function): Option {
    return this.newOption(option, option, translate);
  }

  newOptionForObject(option: any, translate: Function): Option {
    return this.newOption(option['value'], (option['label'] || option['title']), translate);
  }

  newOption(value: string, label: string, translate: Function): Option {
    return new Option(value, translate(label));
  }
}


export class Option {
  constructor(public value: string, public label: string) { }
}
