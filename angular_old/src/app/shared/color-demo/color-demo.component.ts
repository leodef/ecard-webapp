import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-color-demo',
  templateUrl: './color-demo.component.html',
  styleUrls: ['./color-demo.component.css']
})
export class ColorDemoComponent implements OnInit {

  @Input()
  color: String;

  constructor() { }

  ngOnInit() {
  }

}
