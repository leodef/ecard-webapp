import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { MocksModule } from './mocks/mocks.module';
import { routing } from './app.routing';
import { DatePipe } from '@angular/common';
import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    SharedModule,
    CoreModule.forRoot(),
    ...MocksModule.forMocksEnv(),
    routing
  ],
  providers: [
    DatePipe
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
