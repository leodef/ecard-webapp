import { NgModule } from '@angular/core';
import { SharedModule as AppSharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    AppSharedModule
  ],
  declarations: []
})
export class CompanyModule { }
