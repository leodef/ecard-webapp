import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { PersonFolderService } from '../../../../person/core/_services/person-folder.service';
import { Folder } from '../../../../shared/_models/folder';

@Injectable()
export class MockPersonFolderService extends PersonFolderService {

  constructor(protected http: Http) { super(http); }

  byFolder(_id: string): Promise<Array<Folder>> {
    return this.http.get(`assets/_mocks/api/person/folder/byFolder.json`)
      .toPromise()
      .then((req) => req.json() as Array<Folder>);
  }

  all(): Promise<Array<Folder>> {
    return this.http.get(`assets/_mocks/api/person/folder/all.json`)
      .toPromise()
      .then((req) => req.json() as Array<Folder>);
  }

  find(_id: string): Promise<Folder> {
    return this.http.get(`assets/_mocks/api/person/folder/find.json`)
      .toPromise()
      .then((req) => req.json() as Folder);
  }

  delete(_id: string): Promise<Folder> {
    return this.http.get(`assets/_mocks/api/person/folder/folder.json`)
      .toPromise()
      .then((req) => req.json() as Folder);
  }

  create(section: Folder): Promise<Folder> {
    return this.http.get(`assets/_mocks/api/person/folder/folder.json`)
      .toPromise()
      .then((req) => req.json() as Folder);
  }

  update(folder: Folder) {
    return this.http.get(`assets/_mocks/api/person/folder/folder.json`)
      .toPromise()
      .then((req) => req.json() as Folder);
  }

}
