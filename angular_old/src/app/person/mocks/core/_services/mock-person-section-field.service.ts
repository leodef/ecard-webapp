import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { PersonSectionFieldService } from '../../../../person/core/_services/person-section-field.service';
import { SectionField } from '../../../../shared/_models/section-field';

@Injectable()
export class MockPersonSectionFieldService extends PersonSectionFieldService {

  constructor(protected http: Http) { super(http); }

  bySection(_id: string): Promise<Array<SectionField>> {
    return this.http.get(`assets/_mocks/api/person/sectionField/bySection.json`)
      .toPromise()
      .then((req) => req.json() as Array<SectionField>);
  }

  all(_id: string): Promise<Array<SectionField>> {
    return this.http.get(`assets/_mocks/api/person/sectionField/all.json`)
      .toPromise()
      .then((req) => req.json() as Array<SectionField>);
  }

  find(_id: string): Promise<SectionField> {
    return this.http.get(`assets/_mocks/api/person/sectionField/find.json`)
      .toPromise()
      .then((req) => req.json() as SectionField);
  }

  delete(_id: string): Promise<SectionField> {
    return this.http.get(`assets/_mocks/api/person/sectionField/sectionField.json`)
      .toPromise()
      .then((req) => req.json() as SectionField);
  }

  create(section: SectionField): Promise<SectionField> {
    return this.http.get(`assets/_mocks/api/person/sectionField/sectionField.json`)
      .toPromise()
      .then((req) => req.json() as SectionField);
  }

  update(sectionField: SectionField) {
    return this.http.get(`assets/_mocks/api/person/sectionField/sectionField.json`)
      .toPromise()
      .then((req) => req.json() as SectionField);
  }

}
