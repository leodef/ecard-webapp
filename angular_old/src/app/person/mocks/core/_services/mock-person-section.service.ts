import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { PersonSectionService } from '../../../../person/core/_services/person-section.service';
import { Section } from '../../../../shared/_models/section';

@Injectable()
export class MockPersonSectionService extends PersonSectionService {

  constructor(protected http: Http) { super(http); }

  all(): Promise<Array<Section>> {
    return this.http.get(`assets/_mocks/api/person/section/all.json`)
      .toPromise()
      .then((req) => req.json() as Array<Section>);
  }

  find(_id: string): Promise<Section> {
    return this.http.get(`assets/_mocks/api/person/section/find.json`)
      .toPromise()
      .then((req) => req.json() as Section);
  }

  delete(_id: string): Promise<Section> {
    return this.http.get(`assets/_mocks/api/person/section/section.json`)
      .toPromise()
      .then((req) => req.json() as Section);
  }

  create(section: Section): Promise<Section> {
    return this.http.get(`assets/_mocks/api/person/section/section.json`)
      .toPromise()
      .then((req) => req.json() as Section);
  }

  update(section: Section) {
    return this.http.get(`assets/_mocks/api/person/section/section.json`)
      .toPromise()
      .then((req) => req.json() as Section);
  }


}
