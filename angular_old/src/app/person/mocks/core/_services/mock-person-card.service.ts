import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { PersonCardService } from '../../../core/_services/person-card.service';
import { RowCardDTO } from '../../../../shared/_dtos/row-card.dto';
import { Card } from '../../../../shared/_models/card';

@Injectable()
export class MockPersonCardService extends PersonCardService {

  constructor(protected http: Http) { super(http); }

  rowByUser(_id: string): Promise<Array<RowCardDTO>> {
    const cardList: Array<RowCardDTO> = new Array<RowCardDTO>();
    return this.http.get(`assets/_mocks/api/person/card/rowByUser.json`)
      .toPromise()
      .then((req) => req.json() as Array<RowCardDTO>);
  }

  delete(_id: string): Promise<Card> {
    return this.http.get(`assets/mocks/core/_mocks/api/card/card.json`)
      .toPromise()
      .then((req) => req.json() as Card);

  }

  create(card: Card): Promise<Card> {
    return this.http.get(`assets/mocks/core/_mocks/api/card/card.json`)
      .toPromise()
      .then((req) => req.json() as Card);
  }

  update(card: Card): Promise<Card> {
    return this.http.get(`assets/mocks/core/_mocks/api/card/card.json`)
      .toPromise()
      .then((req) => req.json() as Card);
  }

}
