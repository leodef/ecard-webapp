import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { SectionLocation } from '../../../../shared/_models/section-location';
import { PersonSectionLocationService } from '../../../core/_services/person-section-location.service';

@Injectable()
export class MockPersonSectionLocationService extends PersonSectionLocationService {

  constructor(protected http: Http) { super(http); }

  bySection(_id: string): Promise<Array<SectionLocation>> {
    const cardList: Array<SectionLocation> = new Array<SectionLocation>();
    return this.http.get(`assets/_mocks/api/person/sectionLocation/bySection.json`)
      .toPromise()
      .then((req) => req.json() as Array<SectionLocation>);
  }

  byCard(_id: string): Promise<Array<SectionLocation>> {
    const cardList: Array<SectionLocation> = new Array<SectionLocation>();
    return this.http.get(`assets/_mocks/api/person/sectionLocation/byCard.json`)
      .toPromise()
      .then((req) => req.json() as Array<SectionLocation>);
  }

  all(): Promise<Array<SectionLocation>> {
    return this.http.get(`assets/_mocks/api/person/sectionLocation/all.json`)
      .toPromise()
      .then((req) => req.json() as Array<SectionLocation>);
  }

  find(_id: string): Promise<SectionLocation> {
    return this.http.get(`assets/_mocks/api/person/sectionLocation/find.json`)
      .toPromise()
      .then((req) => req.json() as SectionLocation);
  }

  delete(_id: string): Promise<SectionLocation> {
    return this.http.get(`assets/_mocks/api/person/sectionLocation/sectionLocation.json`)
      .toPromise()
      .then((req) => req.json() as SectionLocation);
  }

  create(sectionLocation: SectionLocation): Promise<SectionLocation> {
    return this.http.get(`assets/_mocks/api/person/sectionLocation/sectionLocation.json`)
      .toPromise()
      .then((req) => req.json() as SectionLocation);
  }

  update(sectionLocation: SectionLocation) {
    return this.http.get(`assets/_mocks/api/person/sectionLocation/sectionLocation.json`)
      .toPromise()
      .then((req) => req.json() as SectionLocation);
  }
}
