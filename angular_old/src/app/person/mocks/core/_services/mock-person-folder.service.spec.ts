import { TestBed, inject } from '@angular/core/testing';

import { MockPersonFolderService } from './mock-person-folder.service';

describe('MockPersonFolderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MockPersonFolderService]
    });
  });

  it('should be created', inject([MockPersonFolderService], (service: MockPersonFolderService) => {
    expect(service).toBeTruthy();
  }));
});
