import { TestBed, inject } from '@angular/core/testing';

import { MockPersonSectionService } from './mock-person-section.service';

describe('MockPersonSectionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MockPersonSectionService]
    });
  });

  it('should be created', inject([MockPersonSectionService], (service: MockPersonSectionService) => {
    expect(service).toBeTruthy();
  }));
});
