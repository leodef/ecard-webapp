import { Injectable } from '@angular/core';
import { PersonFolderUserService } from '../../../core/_services/person-folder-user.service';
import { Http } from '@angular/http';
import { FolderUser } from '../../../../shared/_models/folder-user';
import { User } from '../../../../shared/_models/user';
import { Folder } from '../../../../shared/_models/folder';

@Injectable()
export class MockPersonFolderUserService extends PersonFolderUserService {

  constructor(protected http: Http) { super(http); }

  byUser(_id: string): Promise<FolderUser> {
    return this.http.get(`assets/_mocks/api/person/folderUser/byUser.json`)
      .toPromise()
      .then((req) => req.json() as FolderUser);
  }

  createRoot(title: string, user_id: string): Promise<FolderUser> {
    const folder: Folder = { title: title, isRoot: true, parent: null } as Folder;
    const folderUser: FolderUser = { folder: folder, user: { _id: user_id } } as FolderUser;

    return this.http.get(`assets/_mocks/api/person/folderUser/folderUser.json`)
      .toPromise()
      .then((req) => req.json() as FolderUser);
  }

  create(folderUser: FolderUser): Promise<FolderUser> {
    return this.http.get(`assets/_mocks/api/person/folderUser/folderUser.json`)
      .toPromise()
      .then((req) => req.json() as FolderUser);
  }

  update(folderUser: FolderUser): Promise<FolderUser> {
    return this.http.get(`assets/_mocks/api/person/folderUser/folderUser.json`)
      .toPromise()
      .then((req) => req.json() as FolderUser);
  }
}
