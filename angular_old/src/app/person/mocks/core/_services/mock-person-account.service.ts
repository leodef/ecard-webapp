import { Injectable, Inject } from '@angular/core';
import { PersonAccountService } from '../../../core/_services/person-account.service';
import { Http } from '@angular/http';
import { AuthService } from '../../../../core/_services/auth.service';
import { ResetPasswordDTO } from '../../../../shared/_dtos/account/reset-password.dto';
import { ForgotPasswordDTO } from '../../../../shared/_dtos/account/forgot-password.dto';
import { ConfirmAccountDTO } from '../../../../shared/_dtos/account/confirm-account.dto';
import { RegisterDTO } from '../../../../shared/_dtos/account/register.dto';
import { LoginDTO } from '../../../../shared/_dtos/account/login.dto';
import { PersonSessionProfileDTO } from '../../../../shared/_dtos/person-session-profile.dto';
import { CheckUsernameDTO } from '../../../../shared/_dtos/account/check-username.dto';

@Injectable()
export class MockPersonAccountService extends PersonAccountService {

  static serviceName = 'PersonAccountService';

  constructor(
    @Inject(AuthService.serviceName) protected authService: AuthService,
    protected http: Http,
  ) { super(authService, http); }

  public login(loginDTO: LoginDTO): Promise<PersonSessionProfileDTO> {
    return this.http.get(`assets/_mocks/api/person/account/login.json`)
      .toPromise()
      .then(loginResponse => {
        const profile: PersonSessionProfileDTO = loginResponse.json();
        this.authService.login(loginDTO, profile, 'mock-token');
        return profile;
      });
  }

  public logout() {
    return this.http.get(`assets/_mocks/api/person/account/logout.json`)
      .toPromise()
      .then(logoutResponse => {
        this.authService.logout();
        return logoutResponse.json();
      });
  }

  public register(registerDTO: RegisterDTO): Promise<any> {
    return this.http.get(`assets/_mocks/api/person/account/register.json`)
      .toPromise()
      .then(registerResponse => {
        return registerResponse.json();
      });

  }

  public confirmAccount(confirmAccountDTO: ConfirmAccountDTO): Promise<any> {
    return this.http.get(`assets/_mocks/api/person/account/confirmAccount.json`)
      .toPromise()
      .then(confirmAccountResponse => {
        return confirmAccountResponse.json();
      });
  }

  public forgotPassword(forgotPasswordDTO: ForgotPasswordDTO): Promise<any> {
    return this.http.get(`assets/_mocks/api/person/account/forgotPassword.json`)
      .toPromise()
      .then(forgotPasswordResponse => {
        return forgotPasswordResponse.json();
      });
  }

  public resetPassword(resetPasswordDTO: ResetPasswordDTO): Promise<any> {
    return this.http.get(`assets/_mocks/api/person/account/resetPassword.json`)
      .toPromise()
      .then(resetPasswordResponse => {
        return resetPasswordResponse.json();
      });
  }

  public checkUsername(username: string): Promise<CheckUsernameDTO> {
    if (username === 'usedusername') {
      return this.http.get(`assets/_mocks/api/person/account/checkUsername/false.json`)
        .toPromise()
        .then(usernameList => {
          return usernameList.json();
        });
    } else {
      return this.http.get(`assets/_mocks/api/person/account/checkUsername/true.json`)
        .toPromise()
        .then(usernameList => {
          return usernameList.json();
        });
    }
  }
}
