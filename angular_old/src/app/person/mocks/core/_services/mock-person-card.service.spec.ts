import { TestBed, inject } from '@angular/core/testing';

import { MockPersonCardService } from './mock-person-card.service';

describe('MockPersonCardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MockPersonCardService]
    });
  });

  it('should be created', inject([MockPersonCardService], (service: MockPersonCardService) => {
    expect(service).toBeTruthy();
  }));
});
