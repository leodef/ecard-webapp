import { TestBed, inject } from '@angular/core/testing';

import { MockPersonFolderUserService } from './mock-person-folder-user.service';

describe('MockPersonFolderUserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MockPersonFolderUserService]
    });
  });

  it('should be created', inject([MockPersonFolderUserService], (service: MockPersonFolderUserService) => {
    expect(service).toBeTruthy();
  }));
});
