import { TestBed, inject } from '@angular/core/testing';

import { MockPersonSectionFieldService } from './mock-person-section-field.service';

describe('MockPersonSectionFieldService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MockPersonSectionFieldService]
    });
  });

  it('should be created', inject([MockPersonSectionFieldService], (service: MockPersonSectionFieldService) => {
    expect(service).toBeTruthy();
  }));
});
