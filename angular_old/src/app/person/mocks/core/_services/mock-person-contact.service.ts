import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { PersonContactService } from '../../../../person/core/_services/person-contact.service';
import { Contact } from '../../../../shared/_models/contact';

@Injectable()
export class MockPersonContactService extends PersonContactService {

  constructor(protected http: Http) { super(http); }

  byFolder(_id: string): Promise<Array<Contact>> {
    return this.http.get(`assets/_mocks/api/person/contact/byFolder.json`)
      .toPromise()
      .then((req) => req.json() as Array<Contact>);
  }

  all(): Promise<Array<Contact>> {
    return this.http.get(`assets/_mocks/api/person/contact/all.json`)
      .toPromise()
      .then((req) => req.json() as Array<Contact>);
  }

  find(_id: string): Promise<Contact> {
    return this.http.get(`assets/_mocks/api/person/contact/find.json`)
      .toPromise()
      .then((req) => req.json() as Contact);
  }

  delete(_id: string): Promise<Contact> {
    return this.http.get(`assets/_mocks/api/person/contact/contact.json`)
      .toPromise()
      .then((req) => req.json() as Contact);
  }

  create(section: Contact): Promise<Contact> {
    return this.http.get(`assets/_mocks/api/person/contact/contact.json`)
      .toPromise()
      .then((req) => req.json() as Contact);
  }

  update(contact: Contact) {
    return this.http.get(`assets/_mocks/api/person/contact/contact.json`)
      .toPromise()
      .then((req) => req.json() as Contact);
  }


}
