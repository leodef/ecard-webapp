import { TestBed, inject } from '@angular/core/testing';

import { MockPersonAccountService } from './mock-person-account.service';

describe('MockPersonAccountService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MockPersonAccountService]
    });
  });

  it('should be created', inject([MockPersonAccountService], (service: MockPersonAccountService) => {
    expect(service).toBeTruthy();
  }));
});
