import { TestBed, inject } from '@angular/core/testing';

import { MockPersonContactService } from './mock-person-contact.service';

describe('MockPersonContactService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MockPersonContactService]
    });
  });

  it('should be created', inject([MockPersonContactService], (service: MockPersonContactService) => {
    expect(service).toBeTruthy();
  }));
});
