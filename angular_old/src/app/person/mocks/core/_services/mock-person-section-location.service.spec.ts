import { TestBed, inject } from '@angular/core/testing';

import { MockPersonSectionLocationService } from './mock-person-section-location.service';

describe('MockPersonSectionLocationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MockPersonSectionLocationService]
    });
  });

  it('should be created', inject([MockPersonSectionLocationService], (service: MockPersonSectionLocationService) => {
    expect(service).toBeTruthy();
  }));
});
