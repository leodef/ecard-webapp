import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MockPersonCardService } from './_services/mock-person-card.service';
import { MockPersonSectionService } from './_services/mock-person-section.service';
import { MockPersonSectionLocationService } from './_services/mock-person-section-location.service';

import { MockPersonSectionFieldService } from './_services/mock-person-section-field.service';
import { MockPersonContactService } from './_services/mock-person-contact.service';
import { MockPersonFolderService } from './_services/mock-person-folder.service';
import { MockPersonFolderUserService } from './_services/mock-person-folder-user.service';
import { MockPersonAccountService } from './_services/mock-person-account.service';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    { provide: MockPersonAccountService.serviceName, useClass: MockPersonAccountService },
    { provide: MockPersonCardService.serviceName, useClass: MockPersonCardService },
    { provide: MockPersonSectionService.serviceName, useClass: MockPersonSectionService },
    { provide: MockPersonSectionLocationService.serviceName, useClass: MockPersonSectionLocationService },
    { provide: MockPersonSectionFieldService.serviceName, useClass: MockPersonSectionFieldService },
    { provide: MockPersonContactService.serviceName, useClass: MockPersonContactService },
    { provide: MockPersonFolderService.serviceName, useClass: MockPersonFolderService },
    { provide: MockPersonFolderUserService.serviceName, useClass: MockPersonFolderUserService }

  ]
})
export class CoreModule { }
