import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginDTO } from '../../../shared/_dtos/account/login.dto';
import { PersonAccountService } from '../../core/_services/person-account.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  loginDTO: LoginDTO = new LoginDTO();
  _form: FormGroup;

  constructor(
    @Inject(PersonAccountService.serviceName) private personAccountService: PersonAccountService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute) { }

  get usernameFormControl(): AbstractControl {
    return this._form.get('username');
  }

  get passwordFormControl(): AbstractControl {
    return this._form.get('password');
  }

  ngOnInit() {
    let logout: boolean = this.route.snapshot.data[0] != null && this.route.snapshot.data[0]['logout'];
    if (logout) { this.logout(); }

    this._form = this.formBuilder.group({
      username: [
        this.loginDTO.username,
        [
          Validators.required,
          Validators.maxLength(80)
        ]
      ],
      password: [
        this.loginDTO.password,
        [
          Validators.required,
        ]
      ],
    });
    this._form.valueChanges.subscribe(data => {
      this.loginDTO = data as LoginDTO;
    });

  }

  login() {
    if (this._form.invalid) {
      // Dados inválidos
      return;
    }
    this.personAccountService.login(this.loginDTO).then(
      response => {
        // Login realizado com sucesso
        this.router.navigate(['/']);
      })
      .catch(
        error => {
          // Erro ao realizar o login: ${error}
        }
      );
  }

  logout() {
    this.personAccountService.logout()
      .then(response => {
        // Logout realizado com sucesso
        this.router.navigate(['/']);
      })
      .catch(error => {
        // Erro ao realizar o logout: ${error}
      });
  }
}
