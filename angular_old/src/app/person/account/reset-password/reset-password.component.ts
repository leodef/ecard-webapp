import { Component, OnInit, Inject } from '@angular/core';
import { PersonAccountService } from '../../core/_services/person-account.service';
import { ResetPasswordDTO } from '../../../shared/_dtos/account/reset-password.dto';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  _form: FormGroup;
  token: string;
  resetPasswordDTO: ResetPasswordDTO = new ResetPasswordDTO();

  constructor(
    @Inject(PersonAccountService.serviceName) private personAccountService: PersonAccountService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  static MatchPassword(AC: AbstractControl) {
    const password = AC.get('password').value; // to get value in input tag
    const confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
    if (password !== confirmPassword) {
      AC.get('confirmPassword').setErrors({ MatchPassword: true });
    } else {
      return null;
    }
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.token = params['token'];
    });

    this._form = this.formBuilder.group({
      password: [this.resetPasswordDTO.password, Validators.required],
      confirmPassword: [this.resetPasswordDTO.confirmPassword, Validators.required],
    }, {
        validator: ResetPasswordComponent.MatchPassword
      });
    this._form.valueChanges.subscribe(data => {
      this.resetPasswordDTO = data as ResetPasswordDTO;
    });
  }

  resetPassword(): void {
    if (this._form.invalid) {
      // Dados inválidos
      return;
    }
    this.resetPasswordDTO.token = this.token;
    this.personAccountService.resetPassword(this.resetPasswordDTO)
      .then(response => {
        // Senha resetada com sucesso
        this.router.navigate(['/student']);
      })
      .catch(error => {
        // Não foi possivel resetar a senha
      });
  }
}
