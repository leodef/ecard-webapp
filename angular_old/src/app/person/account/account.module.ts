import { NgModule } from '@angular/core';
import { SharedModule as AppSharedModule } from '../../shared/shared.module';
import { AuthComponent } from './auth/auth.component';
import { ConfirmAccountComponent } from './confirm-account/confirm-account.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegisterComponent } from './register/register.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { routing } from './person-account.routing';

@NgModule({
  imports: [
    AppSharedModule,
    routing
  ],
  declarations: [
    AuthComponent,
    ConfirmAccountComponent,
    ForgotPasswordComponent,
    RegisterComponent,
    ResetPasswordComponent
  ]
})
export class AccountModule { }
