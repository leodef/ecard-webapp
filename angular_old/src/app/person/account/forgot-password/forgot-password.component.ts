import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ForgotPasswordDTO } from '../../../shared/_dtos/account/forgot-password.dto';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonAccountService } from '../../core/_services/person-account.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  forgotPasswordDTO: ForgotPasswordDTO = new ForgotPasswordDTO();
  _form: FormGroup;

  constructor(
    @Inject(PersonAccountService.serviceName) private personAccountService: PersonAccountService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this._form = this.formBuilder.group({
      email: [this.forgotPasswordDTO.email, Validators.required],
    });
    this._form.valueChanges.subscribe(data => {
      this.forgotPasswordDTO = data as ForgotPasswordDTO;
    });
  }

  forgotPassword(): void {
    if (this._form.invalid) {
      // Dados inválidos
      return;
    }
    this.personAccountService.forgotPassword(this.forgotPasswordDTO)
      .then(response => {
        // Um link foi enviado para seu e-mail para resetar sua senha
        this.router.navigate(['/person']);
      })
      .catch(error => {
        // Não foi possivel requisitar mudança de senha
      });
  }
}
