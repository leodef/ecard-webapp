import { Component, OnInit, Inject } from '@angular/core';
import { DatePipe } from '@angular/common';
import { AbstractControl, Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { RegisterDTO } from '../../../shared/_dtos/account/register.dto';
import { PersonAccountService } from '../../core/_services/person-account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  _form: FormGroup;

  registerDTO: RegisterDTO = new RegisterDTO();

  birth: string;

  _dateFormControl: AbstractControl = new FormControl();

  constructor(
    @Inject(PersonAccountService.serviceName) private personAccountService: PersonAccountService,
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  static MatchPassword(AC: AbstractControl) {
    const password = AC.get('password').value; // to get value in input tag
    const confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
    if (password !== confirmPassword) {
      AC.get('confirmPassword').setErrors({ MatchPassword: true });
    } else {
      return null;
    }
  }

  get nameFormControl(): AbstractControl {
    return this._form.get('name');
  }

  get emailFormControl(): AbstractControl {
    return this._form.get('email');
  }

  get usernameFormControl(): AbstractControl {
    return this._form.get('username');
  }

  get passwordFormControl(): AbstractControl {
    return this._form.get('password');
  }
  get confirmPasswordFormControl(): AbstractControl {
    return this._form.get('confirmPassword');
  }

  setDateFormControl(formControl: AbstractControl) {
    this._dateFormControl = formControl;
  }

  ngOnInit() {
    this._form = this.formBuilder.group({
      name: [
        this.registerDTO.name,
        [
          Validators.required,
          Validators.maxLength(80)
        ]
      ],
      email: [this.registerDTO.email,
      [
        Validators.email
      ]
      ],
      username: [
        this.registerDTO.username,
        [
          Validators.required
        ], [
          this.validateUniqueUsername.bind(this)
        ]
      ],
      password: [this.registerDTO.password, Validators.required],
      confirmPassword: ['', Validators.required],
      birth: this._dateFormControl,
    }, {
        validator: RegisterComponent.MatchPassword
      });


    this._form.valueChanges.subscribe(data => {
      this.registerDTO = data as RegisterDTO;
    });
  }
  getFormControl(name: string): AbstractControl {
    const formControl: AbstractControl = this._form.controls[name];

    return formControl;
  }
  register(): void {
    if (this._form.invalid || !this.birth) {
      // Dados inválidos
      return;
    }
    // this.registerDTO.birth = this.datePipe.transform(this.birthDate, 'dd/MM/yyyy');//'yyyy-dd-MM'
    // this.registerDTO.birth = this.birth;
    this.personAccountService.register(this.registerDTO)
      .then(response => {
        // Usuário Cadastrado com sucesso
        this.router.navigate(['/']);
      })
      .catch(error => {
        // Não foi possivel realizar cadastro
      });
  }

  changeDate(birth: string) {
    // this.birth = birth;
  }

  validateUniqueUsername(control: AbstractControl) {
    return this.personAccountService.checkUsername(control.value)
      .then(response => {
        return response.isValid ? null : { duplicateUsername: true };
      });
  }



}
