import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ConfirmAccountDTO } from '../../../shared/_dtos/account/confirm-account.dto';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonAccountService } from '../../core/_services/person-account.service';

@Component({
  selector: 'app-confirm-account',
  templateUrl: './confirm-account.component.html',
  styleUrls: ['./confirm-account.component.css']
})
export class ConfirmAccountComponent implements OnInit {

  confirmAccountDTO: ConfirmAccountDTO = new ConfirmAccountDTO();

  token: string;

  constructor(
    @Inject(PersonAccountService.serviceName) private personAccountService: PersonAccountService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.token = params['token'];
    });
  }

  confirmAccount() {
    if (this.token) {
      this.confirmAccountDTO.token = this.token;
      this.personAccountService.confirmAccount(this.confirmAccountDTO)
        .then(response => {
          // Conta confirmada com sucesso
          this.router.navigate(['/']);
        })
        .catch(error => {
          // Não foi possivel confirmar a conta
        });

    }
  }
}
