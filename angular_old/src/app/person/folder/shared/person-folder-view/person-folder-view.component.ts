import { Component, OnInit, Inject, OnChanges, SimpleChanges, EventEmitter, Input, Output } from '@angular/core';
import { Contact } from '../../../../shared/_models/contact';
import { Folder } from '../../../../shared/_models/folder';
import { PersonContactService } from '../../../core/_services/person-contact.service';
import { PersonFolderService } from '../../../core/_services/person-folder.service';

@Component({
  selector: 'app-person-folder-view',
  templateUrl: './person-folder-view.component.html',
  styleUrls: ['./person-folder-view.component.css']
})
export class PersonFolderViewComponent implements OnInit, OnChanges {

  @Input() folder: Folder = null;

  @Output() openFolder: EventEmitter<Folder> = new EventEmitter<Folder>();

  @Output() openContact: EventEmitter<Contact> = new EventEmitter<Contact>();

  contactList: Array<Contact> = new Array<Contact>();

  folderList: Array<Folder> = new Array<Folder>();

  constructor(
    @Inject(PersonContactService.serviceName) private personContactService: PersonContactService,
    @Inject(PersonFolderService.serviceName) private personFolderService: PersonFolderService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.folder && changes.folder.currentValue) {
      this.loadcontantByFolder();
    }
  }

  onOpenContact(contact: Contact) {
    this.openContact.emit(contact);
  }

  onOpenFolder(folder: Folder) {
    this.openFolder.emit(folder);
  }

  loadcontantByFolder() {
    this.loadContactListByFolder()
      .then((contactListByFolder) => {
        this.loadFolderListByFolder()
          .then((folderListByFolder) => {

          });
      });
  }

  loadContactListByFolder(): Promise<Array<Contact>> {
    return this.personContactService.byFolder(this.folder._id)
      .then((contactListByFolder) => {
        this.contactList = contactListByFolder;
        return contactListByFolder;
      });
  }

  loadFolderListByFolder(): Promise<Array<Folder>> {
    return this.personFolderService.byFolder(this.folder._id)
      .then((folderListByFolder) => {
        this.folderList = folderListByFolder;
        return folderListByFolder;
      });
  }

  newLine(index: number, last: boolean): boolean {
    return (index % 5 === 0 && !last);
  }

}
