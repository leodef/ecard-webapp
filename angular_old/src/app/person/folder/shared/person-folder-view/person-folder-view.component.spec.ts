import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonFolderViewComponent } from './person-folder-view.component';

describe('PersonFolderViewComponent', () => {
  let component: PersonFolderViewComponent;
  let fixture: ComponentFixture<PersonFolderViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonFolderViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonFolderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
