import { NgModule } from '@angular/core';
import { PersonFolderBreadcrumpComponent } from './person-folder-breadcrump/person-folder-breadcrump.component';
import { PersonFolderViewComponent } from './person-folder-view/person-folder-view.component';
import { PersonFolderIconComponent } from './person-folder-icon/person-folder-icon.component';
import { SharedModule as AppPersonSharedModule } from '../../shared/shared.module';
import { SharedModule as ContactSharedModule } from '../../contact/shared/shared.module';
@NgModule({
  imports: [
    AppPersonSharedModule,
    ContactSharedModule
  ],
  declarations: [
    PersonFolderBreadcrumpComponent,
    PersonFolderViewComponent,
    PersonFolderIconComponent
  ],
  exports: [
    PersonFolderBreadcrumpComponent,
    PersonFolderViewComponent,
    PersonFolderIconComponent
  ]
})
export class SharedModule { }
