import { Component, OnInit, Input, Inject, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { Folder } from '../../../../shared/_models/folder';

@Component({
  selector: 'app-person-folder-icon',
  templateUrl: './person-folder-icon.component.html',
  styleUrls: ['./person-folder-icon.component.css']
})
export class PersonFolderIconComponent implements OnInit {

  @Input() folder: Folder = null;

  @Output() open: EventEmitter<Folder> = new EventEmitter<Folder>();

  constructor() { }

  iconClass = {
    'fa': true,
    'fa-folder-o': true,
    'fa-2x': true,
  }; // 'pull-left': true
  ngOnInit() {
  }


  onOpen() {
    this.open.emit(this.folder);
  }
  getTitle(): string {
    return this.folder.title;
  }
}
