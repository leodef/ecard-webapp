import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonFolderIconComponent } from './person-folder-icon.component';

describe('PersonFolderIconComponent', () => {
  let component: PersonFolderIconComponent;
  let fixture: ComponentFixture<PersonFolderIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonFolderIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonFolderIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
