import { Component, OnInit, Output, Input, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Folder } from '../../../../shared/_models/folder';

@Component({
  selector: 'app-person-folder-breadcrump',
  templateUrl: './person-folder-breadcrump.component.html',
  styleUrls: ['./person-folder-breadcrump.component.css']
})
export class PersonFolderBreadcrumpComponent implements OnInit, OnChanges  {

  @Input() folder: Folder = null;

  @Output()  back: EventEmitter<Folder> = new EventEmitter<Folder>();
  
  folderList: Array<Folder> = new Array<Folder>();
  
  constructor() { }

  ngOnInit() { }
  
  ngOnChanges(changes: SimpleChanges) {
    if(changes.folder && changes.folder.currentValue){
      this.folderList.push(changes.folder.currentValue);
    }
  }
  
  getTitle(folder: Folder){
    return folder.title;
  }
  
  onBack(index: number, last: boolean) {
    if(last){return;}
    //index++;
    this.folder = this.folderList[index];
    this.folderList.length = index;
    this.back.emit(this.folder);
  }
}
