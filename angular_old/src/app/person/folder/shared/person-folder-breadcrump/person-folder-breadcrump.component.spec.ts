import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonFolderBreadcrumpComponent } from './person-folder-breadcrump.component';

describe('PersonFolderBreadcrumpComponent', () => {
  let component: PersonFolderBreadcrumpComponent;
  let fixture: ComponentFixture<PersonFolderBreadcrumpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonFolderBreadcrumpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonFolderBreadcrumpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
