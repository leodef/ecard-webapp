import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonCardService } from './_services/person-card.service';
import { PersonSectionService } from './_services/person-section.service';
import { PersonSectionLocationService } from './_services/person-section-location.service';
import { PersonSectionFieldService } from './_services/person-section-field.service';
import { PersonContactService } from './_services/person-contact.service';
import { PersonFolderService } from './_services/person-folder.service';
import { PersonFolderUserService } from './_services/person-folder-user.service';
import { PersonAccountService } from './_services/person-account.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    { provide: PersonAccountService.serviceName, useClass: PersonAccountService },
    { provide: PersonCardService.serviceName, useClass: PersonCardService },
    { provide: PersonSectionService.serviceName, useClass: PersonSectionService },
    { provide: PersonSectionLocationService.serviceName, useClass: PersonSectionLocationService },
    { provide: PersonSectionFieldService.serviceName, useClass: PersonSectionFieldService },
    { provide: PersonContactService.serviceName, useClass: PersonContactService },
    { provide: PersonFolderService.serviceName, useClass: PersonFolderService },
    { provide: PersonFolderUserService.serviceName, useClass: PersonFolderUserService }
  ]
})
export class CoreModule {

  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: []
    };
  }
}
