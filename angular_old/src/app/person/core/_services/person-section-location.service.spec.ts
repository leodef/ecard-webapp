import { TestBed, inject } from '@angular/core/testing';

import { PersonSectionLocationService } from './person-section-location.service';

describe('PersonSectionLocationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PersonSectionLocationService]
    });
  });

  it('should be created', inject([PersonSectionLocationService], (service: PersonSectionLocationService) => {
    expect(service).toBeTruthy();
  }));
});
