import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { RowCardDTO } from '../../../shared/_dtos/row-card.dto';
import { Card } from '../../../shared/_models/card';

@Injectable()
export class PersonCardService {

  static serviceName = 'PersonCardService';

  constructor(protected http: Http) { }

  url(): string { return `http://localhost:8080/api/person/card`; }
  rowByUserURL(_id: string = ':_id'): string { return `${this.url()}/rowByUser/${_id}/`; }
  allURL(): string { return `/${this.url()}/`; }
  findURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }
  deleteURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }
  createURL(): string { return `/${this.url()}/`; }
  updateURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }

  rowByUser(_id: string): Promise<Array<RowCardDTO>> {
    return this.http.get(`${this.url}/rowByUser/${_id}/`)
      .toPromise()
      .then((req) => req.json() as Array<RowCardDTO>);
  }

  delete(_id: string): Promise<Card> {
    return this.http.delete(`${this.url}/${_id}/`)
      .toPromise()
      .then((req) => req.json() as Card);
  }

  create(card: Card): Promise<Card> {
    return this.http.post(`${this.url}/`, card)
      .toPromise()
      .then((req) => req.json() as Card);
  }

  update(card: Card): Promise<Card> {
    return this.http.put(`${this.url}/${card._id}/`, card)
      .toPromise()
      .then((req) => req.json() as Card);
  }

}
