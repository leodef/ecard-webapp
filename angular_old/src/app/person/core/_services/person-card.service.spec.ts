import { TestBed, inject } from '@angular/core/testing';

import { PersonCardService } from './person-card.service';

describe('PersonCardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PersonCardService]
    });
  });

  it('should be created', inject([PersonCardService], (service: PersonCardService) => {
    expect(service).toBeTruthy();
  }));
});
