import { TestBed, inject } from '@angular/core/testing';

import { PersonFolderService } from './person-folder.service';

describe('PersonFolderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PersonFolderService]
    });
  });

  it('should be created', inject([PersonFolderService], (service: PersonFolderService) => {
    expect(service).toBeTruthy();
  }));
});
