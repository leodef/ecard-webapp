import { Injectable, Inject } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { LoginDTO } from '../../../shared/_dtos/account/login.dto';
import { AuthService } from '../../../core/_services/auth.service';
import { RegisterDTO } from '../../../shared/_dtos/account/register.dto';
import { ConfirmAccountDTO } from '../../../shared/_dtos/account/confirm-account.dto';
import { ForgotPasswordDTO } from '../../../shared/_dtos/account/forgot-password.dto';
import { ResetPasswordDTO } from '../../../shared/_dtos/account/reset-password.dto';
import { PersonSessionProfileDTO } from '../../../shared/_dtos/person-session-profile.dto';
import { CheckUsernameDTO } from '../../../shared/_dtos/account/check-username.dto';

@Injectable()
export class PersonAccountService {

  static serviceName = 'PersonAccountService';

  constructor(
    @Inject(AuthService.serviceName) protected authService: AuthService,
    protected http: Http,
  ) { }

  // URLs
  url(): string { return `http://localhost:8080/api/person/account`; }
  loginURL(): string { return `${this.url()}/login/`; }
  logoutURL(): string { return `${this.url()}/logout/`; }
  registerURL(): string { return `${this.url()}/register/`; }
  confirmAccountURL(token: string = ':token'): string { return `${this.url()}/confirm-account/${token}`; }
  forgotPasswordURL(): string { return `${this.url()}/forgot-password`; }
  resetPasswordURL(token: string = ':token'): string { return `${this.url()}/reset-password/${token}`; }
  checkUsernameURL(username: string = ':username'): string { return `${this.url()}/check-username/${username}`; }

  // methods
  private getLoginParams(loginDTO: LoginDTO) {
    const headers: Headers = new Headers();
    headers.append('Authorization', 'Basic ' + btoa(loginDTO.username + ':' + loginDTO.password));
    return new RequestOptions({ headers: headers, withCredentials: true });
  }

  public login(loginDTO: LoginDTO): Promise<PersonSessionProfileDTO> {
    return this.http.post(this.loginURL(), null, this.getLoginParams(loginDTO))
      .toPromise()
      .then(loginResponse => {
        const profile: PersonSessionProfileDTO = loginResponse.json();
        this.authService.login(loginDTO, profile, loginResponse.headers);
        return profile;
      });
  }

  public logout(): Promise<any> {
    return this.http.post(this.logoutURL(), null)
      .toPromise()
      .then(logoutResponse => {
        this.authService.logout();
        return logoutResponse.json();
      });
  }

  public register(registerDTO: RegisterDTO): Promise<any> {
    registerDTO.confirmAccountURL = this.confirmAccountURL();
    return this.http.post(this.registerURL(), registerDTO)
      .toPromise()
      .then(registerResponse => {
        return registerResponse.json();
      });

  }

  public confirmAccount(confirmAccountDTO: ConfirmAccountDTO): Promise<any> {
    return this.http.post(this.confirmAccountURL(confirmAccountDTO.token), confirmAccountDTO)
      .toPromise()
      .then(confirmAccountResponse => {
        return confirmAccountResponse.json();
      });
  }

  public forgotPassword(forgotPasswordDTO: ForgotPasswordDTO): Promise<any> {
    forgotPasswordDTO.resetPasswordURL = this.resetPasswordURL();
    return this.http.post(this.forgotPasswordURL(), forgotPasswordDTO)
      .toPromise()
      .then(forgotPasswordResponse => {
        return forgotPasswordResponse.json();
      });
  }

  public resetPassword(resetPasswordDTO: ResetPasswordDTO): Promise<any> {
    return this.http.post(this.resetPasswordURL(resetPasswordDTO.token), resetPasswordDTO)
      .toPromise()
      .then(resetPasswordResponse => {
        return resetPasswordResponse.json();
      });
  }

  public checkUsername(username: string): Promise<CheckUsernameDTO> {
    return this.http.get(this.checkUsernameURL(username))
      .toPromise()
      .then(usernameList => {
        return usernameList.json();
      });
  }
}
