import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { SectionField } from '../../../shared/_models/section-field';

@Injectable()
export class PersonSectionFieldService {

  static serviceName = 'PersonSectionFieldService';

  constructor(protected http: Http) { }

  url(): string { return `http://localhost:8080/api/person/sectionField`; }
  bySectionURL(_id: string = ':_id'): string { return `${this.url}/bySection/${_id}/`; }
  allURL(): string { return `/${this.url()}/`; }
  findURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }
  deleteURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }
  createURL(): string { return `/${this.url()}/`; }
  updateURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }


  bySection(_id: string): Promise<Array<SectionField>> {
    return this.http.get(this.bySectionURL(_id))
      .toPromise()
      .then((req) => req.json() as Array<SectionField>);
  }

  all(_id: string): Promise<Array<SectionField>> {
    return this.http.get(this.allURL())
      .toPromise()
      .then((req) => req.json() as Array<SectionField>);
  }

  find(_id: string): Promise<SectionField> {
    return this.http.get(this.findURL(_id))
      .toPromise()
      .then((req) => req.json() as SectionField);
  }

  delete(_id: string): Promise<SectionField> {
    return this.http.delete(this.deleteURL(_id))
      .toPromise()
      .then((req) => req.json() as SectionField);
  }

  create(sectionField: SectionField): Promise<SectionField> {
    return this.http.post(this.createURL(), sectionField)
      .toPromise()
      .then((req) => req.json() as SectionField);
  }

  update(sectionField: SectionField) {
    return this.http.put(this.updateURL(sectionField._id), sectionField)
      .toPromise()
      .then((req) => req.json() as SectionField);
  }
}
