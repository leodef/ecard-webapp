import { TestBed, inject } from '@angular/core/testing';

import { PersonSectionService } from './person-section.service';

describe('PersonSectionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PersonSectionService]
    });
  });

  it('should be created', inject([PersonSectionService], (service: PersonSectionService) => {
    expect(service).toBeTruthy();
  }));
});
