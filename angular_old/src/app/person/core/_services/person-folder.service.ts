import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Folder } from '../../../shared/_models/folder';

@Injectable()
export class PersonFolderService {

  static serviceName = 'PersonFolderService';

  constructor(protected http: Http) { }

  url(): string { return `http://localhost:8080/api/person/folder`; }
  byFolderURL(_id: string = ':_id'): string { return `${this.url}/byFolder/${_id}/`; }
  allURL(): string { return `/${this.url()}/`; }
  findURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }
  deleteURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }
  createURL(): string { return `/${this.url()}/`; }
  updateURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }

  byFolder(_id: string): Promise<Array<Folder>> {
    return this.http.get(this.byFolderURL(_id))
      .toPromise()
      .then((req) => req.json() as Array<Folder>);
  }

  all(): Promise<Array<Folder>> {
    return this.http.get(this.allURL())
      .toPromise()
      .then((req) => req.json() as Array<Folder>);
  }

  find(_id: string): Promise<Folder> {
    return this.http.get(this.findURL(_id))
      .toPromise()
      .then((req) => req.json() as Folder);
  }

  delete(_id: string): Promise<Folder> {
    return this.http.delete(this.deleteURL(_id))
      .toPromise()
      .then((req) => req.json() as Folder);
  }

  create(folder: Folder): Promise<Folder> {
    return this.http.post(this.createURL(), folder)
      .toPromise()
      .then((req) => req.json() as Folder);
  }

  update(folder: Folder) {
    return this.http.put(this.updateURL(folder._id), folder)
      .toPromise()
      .then((req) => req.json() as Folder);
  }

}
