import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Contact } from '../../../shared/_models/contact';

@Injectable()
export class PersonContactService {

  static serviceName = 'PersonContactService';

  constructor(protected http: Http) { }

  url(): string { return `http://localhost:8080/api/person/contact`; }
  byFolderURL(_id: string = ':_id'): string { return `${this.url()}/byFolder/${_id}/`; }
  allURL(): string { return `/${this.url()}/`; }
  findURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }
  deleteURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }
  createURL(): string { return `/${this.url()}/`; }
  updateURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }


  byFolder(_id: string): Promise<Array<Contact>> {
    return this.http.get(`${this.url}/byFolder/${_id}/`)
      .toPromise()
      .then((req) => req.json() as Array<Contact>);
  }

  all(): Promise<Array<Contact>> {
    return this.http.get(`${this.url}/`)
      .toPromise()
      .then((req) => req.json() as Array<Contact>);
  }

  find(_id: string): Promise<Contact> {
    return this.http.get(`${this.url}/${_id}/`)
      .toPromise()
      .then((req) => req.json() as Contact);
  }

  delete(_id: string): Promise<Contact> {
    return this.http.delete(`${this.url}/${_id}/`)
      .toPromise()
      .then((req) => req.json() as Contact);
  }

  create(contact: Contact): Promise<Contact> {
    return this.http.post(`${this.url}/`, contact)
      .toPromise()
      .then((req) => req.json() as Contact);
  }

  update(contact: Contact) {
    return this.http.put(`${this.url}/${contact._id}/`, contact)
      .toPromise()
      .then((req) => req.json() as Contact);
  }
}
