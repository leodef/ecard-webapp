import { TestBed, inject } from '@angular/core/testing';

import { PersonSectionFieldService } from './person-section-field.service';

describe('PersonSectionFieldService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PersonSectionFieldService]
    });
  });

  it('should be created', inject([PersonSectionFieldService], (service: PersonSectionFieldService) => {
    expect(service).toBeTruthy();
  }));
});
