import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { FolderUser } from '../../../shared/_models/folder-user';
import { User } from '../../../shared/_models/user';
import { Folder } from '../../../shared/_models/folder';

@Injectable()
export class PersonFolderUserService {

  static serviceName = 'PersonFolderUserService';

  constructor(protected http: Http) { }

  url(): string { return `http://localhost:8080/api/person/folderUser`; }

  byUserURL(_id: string = ':_id'): string { return `${this.url()}/byUser/${_id}/`; }

  createURL(): string { return `${this.url()}/`; }

  createRootURL(): string { return `${this.url()}/root/`; }

  updateURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }

  byUser(_id: string): Promise<FolderUser> {
    return this.http.get(this.byUserURL(_id))
      .toPromise()
      .then((req) => req.json() as FolderUser);
  }

  createRoot(title: string, user_id: string): Promise<FolderUser> {
    const folder: Folder = { title: title, isRoot: true, parent: null } as Folder;
    const folderUser: FolderUser = { folder: folder, user: { _id: user_id } } as FolderUser;

    return this.http.post(this.createRootURL(), folderUser)
      .toPromise()
      .then((req) => req.json() as FolderUser);
  }

  create(folderUser: FolderUser): Promise<FolderUser> {
    return this.http.post(this.createURL(), folderUser)
      .toPromise()
      .then((req) => req.json() as FolderUser);
  }

  update(folderUser: FolderUser): Promise<FolderUser> {
    return this.http.put(this.updateURL(folderUser._id), folderUser)
      .toPromise()
      .then((req) => req.json() as FolderUser);
  }
}
