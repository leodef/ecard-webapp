import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Section } from '../../../shared/_models/section';

@Injectable()
export class PersonSectionService {

  static serviceName = 'PersonSectionService';

  constructor(protected http: Http) { }

  url(): string { return `http://localhost:8080/api/person/section`; }
  allURL(): string { return `/${this.url()}/`; }
  findURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }
  deleteURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }
  createURL(): string { return `/${this.url()}/`; }
  updateURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }

  all(_id: string): Promise<Array<Section>> {
    return this.http.get(this.allURL())
      .toPromise()
      .then((req) => req.json() as Array<Section>);
  }

  find(_id: string): Promise<Section> {
    return this.http.get(this.findURL())
      .toPromise()
      .then((req) => req.json() as Section);
  }

  delete(_id: string): Promise<Section> {
    return this.http.delete(this.deleteURL(_id))
      .toPromise()
      .then((req) => req.json() as Section);
  }

  create(section: Section): Promise<Section> {
    return this.http.post(this.createURL(), section)
      .toPromise()
      .then((req) => req.json() as Section);
  }

  update(section: Section) {
    return this.http.put(this.updateURL(section._id), section)
      .toPromise()
      .then((req) => req.json() as Section);
  }
}
