import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { SectionLocation } from '../../../shared/_models/section-location';
import { Section } from '../../../shared/_models/section';
import { Card } from '../../../shared/_models/card';
import * as _ from 'lodash';
@Injectable()
export class PersonSectionLocationService {

  static serviceName = 'PersonSectionLocationService';


  constructor(protected http: Http) { }

  url(): string { return `http://localhost:8080/api/person/sectionLocation`; }
  bySectionURL(_id: string = ':_id'): string { return `${this.url}/bySection/${_id}/`; }
  byCardURL(_id: string = ':_id'): string { return `${this.url}/byCard/${_id}/`; }
  allURL(): string { return `/${this.url()}/`; }
  findURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }
  deleteURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }
  createURL(): string { return `/${this.url()}/`; }
  updateURL(_id: string = ':_id'): string { return `${this.url()}/${_id}/`; }


  bySection(_id: string): Promise<Array<SectionLocation>> {
    return this.http.get(this.bySectionURL(_id))
      .toPromise()
      .then((req) => req.json() as Array<SectionLocation>);
  }

  byCard(_id: string): Promise<Array<SectionLocation>> {
    const cardList: Array<SectionLocation> = new Array<SectionLocation>();
    return this.http.get(this.byCardURL(_id))
      .toPromise()
      .then((req) => req.json() as Array<SectionLocation>);
  }

  all(): Promise<Array<SectionLocation>> {
    return this.http.get(this.allURL())
      .toPromise()
      .then((req) => req.json() as Array<SectionLocation>);
  }

  find(_id: string): Promise<SectionLocation> {
    return this.http.get(this.findURL(_id))
      .toPromise()
      .then((req) => req.json() as SectionLocation);
  }

  delete(_id: string): Promise<SectionLocation> {
    return this.http.delete(this.deleteURL(_id))
      .toPromise()
      .then((req) => req.json() as SectionLocation);
  }

  create(sectionLocation: SectionLocation, parent: Card | Section): Promise<SectionLocation> {
    const parentId: string = parent._id;
    let parentTypeId: string = null;

    if (parent instanceof Card) {
      parentTypeId = 'card_id';
    } else if (parent instanceof Section) {
      parentTypeId = 'section_id';
    }
    sectionLocation = _.merge(sectionLocation, { parentTypeId: parentId });
    return this.http.post(this.createURL(), sectionLocation)
      .toPromise()
      .then((req) => req.json() as SectionLocation);
  }

  update(sectionLocation: SectionLocation) {
    return this.http.put(this.updateURL(sectionLocation._id), sectionLocation)
      .toPromise()
      .then((req) => req.json() as SectionLocation);
  }

}
