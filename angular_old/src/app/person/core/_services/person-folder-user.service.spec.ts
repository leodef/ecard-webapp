import { TestBed, inject } from '@angular/core/testing';

import { PersonFolderUserService } from './person-folder-user.service';

describe('PersonFolderUserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PersonFolderUserService]
    });
  });

  it('should be created', inject([PersonFolderUserService], (service: PersonFolderUserService) => {
    expect(service).toBeTruthy();
  }));
});
