import { TestBed, inject } from '@angular/core/testing';

import { PersonAccountService } from './person-account.service';

describe('PersonAccountService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PersonAccountService]
    });
  });

  it('should be created', inject([PersonAccountService], (service: PersonAccountService) => {
    expect(service).toBeTruthy();
  }));
});
