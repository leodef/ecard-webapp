import { TestBed, inject } from '@angular/core/testing';

import { PersonContactService } from './person-contact.service';

describe('PersonContactService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PersonContactService]
    });
  });

  it('should be created', inject([PersonContactService], (service: PersonContactService) => {
    expect(service).toBeTruthy();
  }));
});
