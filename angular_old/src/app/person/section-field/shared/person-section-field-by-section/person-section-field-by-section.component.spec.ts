import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonSectionFieldBySectionComponent } from './person-section-field-by-section.component';

describe('PersonSectionFieldBySectionComponent', () => {
  let component: PersonSectionFieldBySectionComponent;
  let fixture: ComponentFixture<PersonSectionFieldBySectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonSectionFieldBySectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonSectionFieldBySectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
