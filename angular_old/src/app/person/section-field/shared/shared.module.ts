import { NgModule } from '@angular/core';
import { PersonSectionFieldBySectionComponent } from './person-section-field-by-section/person-section-field-by-section.component';
import { SharedModule as AppPersonSharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    AppPersonSharedModule
  ],
  declarations: [
    PersonSectionFieldBySectionComponent
  ],
  exports: [
    PersonSectionFieldBySectionComponent
  ]
})
export class SharedModule { }
