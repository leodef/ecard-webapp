import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '../core/_services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  get authenticated(): boolean{
    return this.authService.isAuthenticated;
  }

  set authenticated(authenticated :boolean){   }

  constructor(
    @Inject(AuthService.serviceName) protected authService: AuthService,
    private router:Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    let person_root:boolean = this.route.snapshot.data[0] != null && this.route.snapshot.data[0]['person_root'];

    if(person_root && this.authenticated){ 
      this.goToDashboard();
    }
    
  }
  goToDashboard(){
    this.router.navigate(['./dashboard']);
  }


}
