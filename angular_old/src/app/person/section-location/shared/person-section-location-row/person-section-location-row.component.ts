import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Section } from '../../../../shared/_models/section';
import { SectionLocation } from '../../../../shared/_models/section-location';

@Component({
  selector: 'app-person-section-location-row',
  templateUrl: './person-section-location-row.component.html',
  styleUrls: ['./person-section-location-row.component.css']
})
export class PersonSectionLocationRowComponent implements OnInit {

  @Input() sectionLocation: SectionLocation = null;

  @Output() select: EventEmitter<SectionLocation> = new EventEmitter<SectionLocation>();

  @Output() delete: EventEmitter<SectionLocation> = new EventEmitter<SectionLocation>();

  get sectionLocationSelected(): boolean {
    if (this.sectionLocation && this.sectionLocation._id) {
      return true;
    }
    return false;
  }

  set sectionLocationSelected(sectionSelected: boolean) { }

  get section(): Section {
    if (!this.sectionLocationSelected) { return null; }
    return this.sectionLocation.section;
  }

  set section(section: Section) { }

  get backgroundColor(): string {
    return this.section.color;
  }
  set backgroundColor(backgroundColor: string) { }

  constructor() { }

  ngOnInit() {
  }

  selectClick() {
    this.select.emit(this.sectionLocation);
  }
  deleteClick() {
    this.delete.emit(this.sectionLocation);
  }

}
