import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonSectionLocationRowComponent } from './person-section-location-row.component';

describe('PersonSectionLocationRowComponent', () => {
  let component: PersonSectionLocationRowComponent;
  let fixture: ComponentFixture<PersonSectionLocationRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonSectionLocationRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonSectionLocationRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
