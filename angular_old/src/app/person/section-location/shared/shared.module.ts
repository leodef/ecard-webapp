import { NgModule } from '@angular/core';
import { PersonSectionLocationByCardComponent } from './person-section-location-by-card/person-section-location-by-card.component';
import { PersonSectionLocationInfoComponent } from './person-section-location-info/person-section-location-info.component';
import { PersonSectionLocationEditComponent } from './person-section-location-edit/person-section-location-edit.component';
import { PersonSectionLocationBreadcrumbComponent } from './person-section-location-breadcrump/person-section-location-breadcrumb.component';
import { PersonSectionLocationRowComponent } from './person-section-location-row/person-section-location-row.component';
import { PersonSectionLocationFormComponent } from './person-section-location-form/person-section-location-form.component';
import { SharedModule as AppPersonSharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    AppPersonSharedModule,
 ],
 exports: [
    PersonSectionLocationByCardComponent,
    PersonSectionLocationInfoComponent,
    PersonSectionLocationEditComponent,
    PersonSectionLocationBreadcrumbComponent,
    PersonSectionLocationRowComponent,
    PersonSectionLocationFormComponent
 ],
 declarations: [
    PersonSectionLocationByCardComponent,
    PersonSectionLocationInfoComponent,
    PersonSectionLocationEditComponent,
    PersonSectionLocationBreadcrumbComponent,
    PersonSectionLocationRowComponent,
    PersonSectionLocationFormComponent
 ]
})
export class SharedModule { }
