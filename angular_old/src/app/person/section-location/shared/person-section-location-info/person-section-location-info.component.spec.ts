import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonSectionLocationInfoComponent } from './person-section-location-info.component';

describe('PersonSectionLocationInfoComponent', () => {
  let component: PersonSectionLocationInfoComponent;
  let fixture: ComponentFixture<PersonSectionLocationInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonSectionLocationInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonSectionLocationInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
