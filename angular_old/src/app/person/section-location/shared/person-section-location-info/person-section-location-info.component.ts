import { Component, OnInit, Input } from '@angular/core';
import { Section } from '../../../../shared/_models/section';

@Component({
  selector: 'app-person-section-location-info',
  templateUrl: './person-section-location-info.component.html',
  styleUrls: ['./person-section-location-info.component.css']
})
export class PersonSectionLocationInfoComponent implements OnInit {

  @Input() section: Section = null;

  constructor() { }

  ngOnInit() {
  }

}
