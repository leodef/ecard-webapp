import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonSectionLocationByCardComponent } from './person-section-location-by-card.component';

describe('PersonSectionLocationByCardComponent', () => {
  let component: PersonSectionLocationByCardComponent;
  let fixture: ComponentFixture<PersonSectionLocationByCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonSectionLocationByCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonSectionLocationByCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
