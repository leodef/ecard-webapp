import { Component, OnInit, Input, Output, Inject, SimpleChanges } from '@angular/core';
import { PersonSectionLocationService } from '../../../core/_services/person-section-location.service';
import { Section } from '../../../../shared/_models/section';
import { SectionLocation } from '../../../../shared/_models/section-location';
import { Card } from '../../../../shared/_models/card';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-person-section-location-by-card',
  templateUrl: './person-section-location-by-card.component.html',
  styleUrls: ['./person-section-location-by-card.component.css']
})
export class PersonSectionLocationByCardComponent implements OnInit, OnChanges {

  @Input() card: Card;

  sectionLocation: SectionLocation = null;

  sectionLocationList: Array<SectionLocation> = new Array<SectionLocation>();

  get sectionLocationSelected(): boolean {
    if (this.sectionLocation && this.sectionLocation._id) {
      return true;
    }
    return false;
  }

  set sectionLocationSelected(selected: boolean) { }

  get backgroundColor(): string {
    return this.section.color;
  }

  set backgroundColor(backgroundColor: string) { }

  get section(): Section {
    if (!this.sectionLocationSelected) { return null; }
    return this.sectionLocation.section;
  }

  set section(section: Section) { }

  get hasCard(): boolean {
    if (this.card || this.card._id) {
      return true;
    }
    return false;
  }

  set hasCard(hasCard: boolean) { }

  constructor(
    @Inject(PersonSectionLocationService.serviceName) private personSectionLocationService: PersonSectionLocationService
  ) { }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.card && changes.card.currentValue) {
      this.loadSections();
    }
  }

  loadSections() {
    if (this.sectionLocationSelected && this.sectionLocation._id) {
      this.loadSectionSections();
    } else if (this.hasCard && this.card._id) {
      this.loadCardSections();
    }
  }

  loadCardSections() {
    this.personSectionLocationService.byCard(this.card._id)
      .then(
        (sectionLocationList: Array<SectionLocation>) => {
          this.sectionLocationList = sectionLocationList.sort(SectionLocation.compare);
        });
  }

  loadSectionSections() {
    this.personSectionLocationService.bySection(this.card._id)
      .then(
        (sectionLocationList: Array<SectionLocation>) => {
          this.sectionLocationList = sectionLocationList.sort(SectionLocation.compare);
        });
  }


  selectRow(sectionLocation: SectionLocation) {
    if (sectionLocation) {
      this.sectionLocation = sectionLocation;
      this.loadSections();
    }
  }

  deleteRow(sectionLocation: SectionLocation) {
    this.personSectionLocationService.delete(sectionLocation._id)
      .then(
        (deletedSectionLocation: SectionLocation) => {
          this.loadSections();
        });

  }

  openCreateRow() {

  }
  createRow(sectionLocation: SectionLocation) {
    const parent: Card | Section = (this.sectionLocationSelected) ? this.section : this.card;
    this.personSectionLocationService.create(sectionLocation, parent)
      .then((sectionLocationCreated) => {

      });
  }

  openEdit() {

  }
  edit() {

  }

  back(sectionLocation: SectionLocation) {
    this.sectionLocation = sectionLocation;
    this.loadSections();
  }

  backAll() {
    this.sectionLocation = null;
    this.loadSections();
  }
}
