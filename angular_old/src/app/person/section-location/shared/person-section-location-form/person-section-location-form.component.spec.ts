import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonSectionLocationFormComponent } from './person-section-location-form.component';

describe('PersonSectionLocationFormComponent', () => {
  let component: PersonSectionLocationFormComponent;
  let fixture: ComponentFixture<PersonSectionLocationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonSectionLocationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonSectionLocationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
