import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges  } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Section } from '../../../../shared/_models/section';
import * as _ from 'lodash';

@Component({
  selector: 'app-person-section-location-form',
  templateUrl: './person-section-location-form.component.html',
  styleUrls: ['./person-section-location-form.component.css']
})
export class PersonSectionLocationFormComponent implements OnInit {

  _form: FormGroup = null;

  @Input() section: Section = null;

  editCode: boolean = false;

  @Output() sectionChange: EventEmitter<Section> = new EventEmitter<Section>();

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.section = this.section || new Section();
    this._form = this.formBuilder.group({
      title: [
        this.section.title,
        [
          Validators.required,
          Validators.maxLength(50)
        ]
      ],
      columnN: this.section.columnN,
      rowN: this.section.rowN
    });
    this._form.valueChanges.subscribe(data => {
      this.emitChanges(data);
    });

  }

  emitChanges(data: any) {
    /*
    color: string;
    orientation: string;
    columnN: number;
    rowN: number;
    */
    this.section = _.merge(this.section, data);
    this.sectionChange.emit(this.section);
  }

  isNew(): Boolean {
    if (this.section._id == null) {
      return false;
    }
    return true;
  }

}
