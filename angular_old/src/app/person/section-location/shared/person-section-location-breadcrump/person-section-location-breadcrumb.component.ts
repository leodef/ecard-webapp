import { Component, OnInit, Output, Input, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Section } from '../../../../shared/_models/section';
import { Card } from '../../../../shared/_models/card';
import { SectionLocation } from '../../../../shared/_models/section-location';

@Component({
  selector: 'app-person-section-location-breadcrumb',
  templateUrl: './person-section-location-breadcrumb.component.html',
  styleUrls: ['./person-section-location-breadcrumb.component.css']
})
export class PersonSectionLocationBreadcrumbComponent implements OnInit, OnChanges {

  sectionLocationList: Array<SectionLocation> = new Array<SectionLocation>();

  @Output()  back: EventEmitter<SectionLocation> = new EventEmitter<SectionLocation>();

  @Output() backAll: EventEmitter<any> = new EventEmitter<any>();

  @Input() sectionLocation: SectionLocation = null;

  @Input() card: Card = null;

  get sectionLocationSelected(): boolean {
    if(this.sectionLocation && this.sectionLocation._id){
      return true;
    }
    return false;
  }

  set sectionLocationSelected(sectionSelected: boolean){}

  get section(): Section{
    if(!this.sectionLocationSelected){return null;}
    return this.sectionLocation.section;
  }
  
  set section(section: Section){}

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.sectionLocation && changes.sectionLocation.currentValue){
      this.sectionLocationList.push(changes.sectionLocation.currentValue);
    }
  }



  onBack(index: number, last: boolean) {
    if(last){return;}
    index++;
    this.sectionLocation = this.sectionLocationList[index];
    this.sectionLocationList.length = index;
    this.back.emit(this.sectionLocation);
  }

  onBackAll(){
    this.sectionLocation = null;
    this.sectionLocationList.length = 0;
    this.backAll.emit(this.sectionLocation);
  }

  getTitle(sectionLocation: SectionLocation){
    if(!sectionLocation || !sectionLocation.section){return null;}
    return sectionLocation.section.title;
  }

}
