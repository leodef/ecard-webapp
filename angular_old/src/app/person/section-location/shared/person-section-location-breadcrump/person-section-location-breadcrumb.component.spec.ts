import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonSectionLocationBreadcrumpComponent } from './person-section-location-breadcrump.component';

describe('PersonSectionLocationBreadcrumpComponent', () => {
  let component: PersonSectionLocationBreadcrumpComponent;
  let fixture: ComponentFixture<PersonSectionLocationBreadcrumpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonSectionLocationBreadcrumpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonSectionLocationBreadcrumpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
