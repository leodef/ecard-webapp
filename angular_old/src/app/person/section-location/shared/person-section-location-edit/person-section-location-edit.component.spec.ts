import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonSectionLocationEditComponent } from './person-section-location-edit.component';

describe('PersonSectionLocationEditComponent', () => {
  let component: PersonSectionLocationEditComponent;
  let fixture: ComponentFixture<PersonSectionLocationEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonSectionLocationEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonSectionLocationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
