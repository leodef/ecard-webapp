import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Section } from '../../../../shared/_models/section';

@Component({
  selector: 'app-person-section-location-edit',
  templateUrl: './person-section-location-edit.component.html',
  styleUrls: ['./person-section-location-edit.component.css']
})
export class PersonSectionLocationEditComponent implements OnInit {

  @Input() section: Section = null;

  editCode: boolean = false;

  @Output() save: EventEmitter<Section> = new EventEmitter<Section>();

  @Output() cancel: EventEmitter<Section> = new EventEmitter<Section>();

  constructor() { }

  ngOnInit() {
  }

  saveSection() {
    this.save.emit(this.section);
  }

  cancelSection() {
    this.cancel.emit(this.section);
  }

}
