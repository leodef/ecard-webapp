import { NgModule } from '@angular/core';
import { PersonContactIconComponent } from './person-contact-icon/person-contact-icon.component';
import { SharedModule as AppPersonSharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    AppPersonSharedModule
  ],
  declarations: [
    PersonContactIconComponent
  ],
  exports: [
    PersonContactIconComponent
  ]
})
export class SharedModule { }
