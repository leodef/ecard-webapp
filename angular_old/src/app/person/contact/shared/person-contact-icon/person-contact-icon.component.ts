import { Component, OnInit, Input, Inject, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { Contact } from '../../../../shared/_models/contact';
import { Card } from '../../../../shared/_models/card';
import { PersonContactService } from '../../../core/_services/person-contact.service';
import { PersonCardService } from '../../../core/_services/person-card.service';
import CONTACT_TYPE from '../../../../shared/_collections/contact-type';

@Component({
  selector: 'app-person-contact-icon',
  templateUrl: './person-contact-icon.component.html',
  styleUrls: ['./person-contact-icon.component.css']
})
export class PersonContactIconComponent implements OnInit, OnChanges {

  @Input() contact: Contact = null;

  @Output() open: EventEmitter<Contact> = new EventEmitter<Contact>();

  contactContent: Card;

  title = '';
  iconClass = {
    'fa': true,
    'fa-2x': true,
  };
  // 'pull-left': true


  constructor(
    @Inject(PersonContactService.serviceName) private personContactService: PersonContactService,
    @Inject(PersonCardService.serviceName) private personCardService: PersonCardService
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.contact && changes.contact.currentValue) {
      this.loadContact();
    }
  }

  onOpen() {
    this.open.emit(this.contact);
  }

  loadContact() {
    switch (this.contact.contactType) {
      case CONTACT_TYPE.CARD:
        this.loadContactAsCard();
    }
  }

  loadContactAsCard() {
    this.personCardService.find(this.contact.contactId)
      .then((card) => {
        this.contactContent = card;
        this.title = card.title;
        this.iconClass['fa-id-card'] = true;
      });
  }

  getTitle(): string {
    return this.title;
  }

}
