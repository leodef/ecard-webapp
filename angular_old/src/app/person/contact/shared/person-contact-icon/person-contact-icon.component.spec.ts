import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonContactIconComponent } from './person-contact-icon.component';

describe('PersonContactIconComponent', () => {
  let component: PersonContactIconComponent;
  let fixture: ComponentFixture<PersonContactIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonContactIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonContactIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
