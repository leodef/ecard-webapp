import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-person-account-dropdown-menu',
  templateUrl: './person-account-dropdown-menu.component.html',
  styleUrls: ['./person-account-dropdown-menu.component.css']
})
export class PersonAccountDropdownMenuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  goToLogout(){

  }

  goToAccount(){

  }

  goToProfile(){

  }

  goToPrivacity(){
    
  }
}
