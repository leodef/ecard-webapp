import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonAccountDropdownMenuComponent } from './person-account-dropdown-menu.component';

describe('PersonAccountDropdownMenuComponent', () => {
  let component: PersonAccountDropdownMenuComponent;
  let fixture: ComponentFixture<PersonAccountDropdownMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonAccountDropdownMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonAccountDropdownMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
