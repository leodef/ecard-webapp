import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonHeaderPublicComponent } from './person-header-public.component';

describe('PersonHeaderPublicComponent', () => {
  let component: PersonHeaderPublicComponent;
  let fixture: ComponentFixture<PersonHeaderPublicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonHeaderPublicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonHeaderPublicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
