import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-person-header-authenticated',
  templateUrl: './person-header-authenticated.component.html',
  styleUrls: ['./person-header-authenticated.component.css']
})
export class PersonHeaderAuthenticatedComponent implements OnInit {

  search: String;

  constructor() { }

  ngOnInit() {
  }

  goToDashboard(){

  }

  goToSearch(){

  }

}
