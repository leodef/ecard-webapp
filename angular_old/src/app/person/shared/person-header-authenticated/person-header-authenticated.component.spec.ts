import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonHeaderAuthenticatedComponent } from './person-header-authenticated.component';

describe('PersonHeaderAuthenticatedComponent', () => {
  let component: PersonHeaderAuthenticatedComponent;
  let fixture: ComponentFixture<PersonHeaderAuthenticatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonHeaderAuthenticatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonHeaderAuthenticatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
