import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonFooterComponent } from './person-footer.component';

describe('PersonFooterComponent', () => {
  let component: PersonFooterComponent;
  let fixture: ComponentFixture<PersonFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
