import { NgModule } from '@angular/core';
import { SharedModule as AppSharedModule } from '../../shared/shared.module';
import { PersonHeaderPublicComponent } from './person-header-public/person-header-public.component';
import { PersonHeaderAuthenticatedComponent } from './person-header-authenticated/person-header-authenticated.component';
import { PersonFooterComponent } from './person-footer/person-footer.component';
import { PersonAccountDropdownMenuComponent } from './person-account-dropdown-menu/person-account-dropdown-menu.component';

@NgModule({
  imports: [
    AppSharedModule,
  ],
  exports: [
    AppSharedModule,
    PersonHeaderPublicComponent,
    PersonHeaderAuthenticatedComponent,
    PersonFooterComponent,
    PersonAccountDropdownMenuComponent
  ],
  declarations: [
    PersonHeaderPublicComponent,
    PersonHeaderAuthenticatedComponent,
    PersonFooterComponent,
    PersonAccountDropdownMenuComponent
  ]
})
export class SharedModule { }
