import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { PersonComponent } from './person.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MyContactsComponent } from './my-contacts/my-contacts.component';

const routes: Routes = [
  {
    path: '',
    component: PersonComponent,
    data: [{ person_root: true }],
    children: [{ path: '', component: HomeComponent }],
    //pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: PersonComponent,
    data: [{ user_root: false }],
    children: [{ path: '', component: DashboardComponent }]
  },
  {
    path: 'my-contacts',
    component: PersonComponent,
    data: [{ user_root: false }],
    children: [{ path: '', component: MyContactsComponent }]
  },
  { 
    path: 'card',
    component: PersonComponent,
    data: [{admin_root: false}],
    loadChildren: './card/card.module#CardModule'
  },
  { 
    path: 'account',
    component: PersonComponent,
    data: [{admin_root: false}],
    loadChildren: './account/account.module#AccountModule'
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
