import { NgModule } from '@angular/core';
import { routing } from './person.routing';
// shared
import { SharedModule } from './shared/shared.module';
import { SharedModule as PersonCardSharedModule } from './card/shared/shared.module';
import { SharedModule as PersonSectionLocationSharedModule } from './section-location/shared/shared.module';
import { SharedModule as PersonFolderSharedModule } from './folder/shared/shared.module';
import { SharedModule as PersonSectionFieldSharedModule } from './section-field/shared/shared.module';
import { SharedModule as PersonContactSharedModule } from './contact/shared/shared.module';
// core
import { CoreModule } from './core/core.module';
import { MocksModule } from './mocks/mocks.module';
// components
import { PersonComponent } from './person.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MyContactsComponent } from './my-contacts/my-contacts.component';

@NgModule({
  imports: [
    SharedModule,
    PersonCardSharedModule,
    PersonSectionLocationSharedModule,
    PersonFolderSharedModule,
    CoreModule.forRoot(),
    ...MocksModule.forMocksEnv(), // spread operator. it allows you to easily place an expanded version of an array into another array.
    routing
  ],
  declarations: [
    PersonComponent,
    HomeComponent,
    DashboardComponent,
    MyContactsComponent
  ]
})
export class PersonModule { }
