import { Component, OnInit, Inject, TemplateRef, ViewChild } from '@angular/core';
import { Contact } from '../../shared/_models/contact';
import { Folder } from '../../shared/_models/folder';
import { PersonFolderUserService } from '../core/_services/person-folder-user.service';
import { User } from '../../shared/_models/user';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-my-contacts',
  templateUrl: './my-contacts.component.html',
  styleUrls: ['./my-contacts.component.css']
})
export class MyContactsComponent implements OnInit {

  folder: Folder = null;

  modalCreateRootRef: BsModalRef;

  user: User = { _id: 'USR-MNC746-1' } as User;

  get folderLoaded(): boolean {
    if (this.folder) {
      return true;
    }
    return false;
  }

  constructor(
    @Inject(PersonFolderUserService.serviceName) private personFolderUserService: PersonFolderUserService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.loadFolder();
  }

  loadFolder() {
    this.personFolderUserService.byUser(this.user._id)
      .then((folderUser) => {
        if (folderUser) {
          this.folder = folderUser.folder;
        } else {
          this.openCreateRoot();
        }
      });
  }

  openCreateRoot() {
    this.modalCreateRootRef = this.modalService.show(ModalContentComponent);
    this.modalCreateRootRef.content.title = 'Modal with component';
    this.modalCreateRootRef.content.save = this.createRoot;
  }

  createRoot(title: string) {
    this.personFolderUserService.createRoot(title, this.user._id)
      .then((folderUser) => {
        this.folder = folderUser.folder;
      });
  }

  back(folder: Folder) {
    this.folder = folder;
  }

  openFolder(folder: Folder) {
    this.folder = folder;
  }

  openContact(contact: Contact) {

  }


}
/*
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'modal-content',
  templateUrl: './modal-content.component.html',
})
export class ModalContentComponent {
  title: string;

  constructor(public bsModalRef: BsModalRef) { }

  save(title: string) { }
}

*/
