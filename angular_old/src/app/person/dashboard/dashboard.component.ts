import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Card } from '../../shared/_models/card';
import { RowCardDTO } from '../../shared/_dtos/row-card.dto';
import { PersonCardService } from '../core/_services/person-card.service';
import { AuthService } from '../../core/_services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  cardList: Array<RowCardDTO> = new Array<RowCardDTO>();

  constructor(
    @Inject(PersonCardService.serviceName) private personCardService: PersonCardService,
    @Inject(AuthService.serviceName) private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.loadList();
  }

  loadList() {
    this.personCardService.rowByUser(this.authService.accountId.toString())
      .then((cardList) => {
        this.cardList = cardList;
      });
  }

  create() {
    this.personCardService.create(null)
      .then((created) => {

        this.loadList();
      });
  }

  select(card: RowCardDTO) {
    this.router.navigate(['/person/card', card._id]);
  }

  delete(card: RowCardDTO) {
    this.personCardService.delete(card._id)
      .then((deleted) => {

        this.loadList();
      });
  }
}
