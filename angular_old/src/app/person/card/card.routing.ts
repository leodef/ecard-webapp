import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { CardViewComponent } from './card-view/card-view.component';

const routes: Routes = [
  {
    path: ':id',
    component: CardViewComponent,
    data: [{ person_root: true }]
    //pathMatch: 'full'
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
