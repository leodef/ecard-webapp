import { Component, OnInit, Inject, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonCardService } from '../../core/_services/person-card.service';
import { Card } from '../../../shared/_models/card';

@Component({
  selector: 'app-card-view',
  templateUrl: './card-view.component.html',
  styleUrls: ['./card-view.component.css']
})
export class CardViewComponent implements OnInit {

  edit = false;

  test = '';

  card: Card = new Card();

  modalEditRef: BsModalRef;

  get backgroundColor(): string {
    return this.card.color;
  }

  set backgroundColor(backgroundColor: string) { }

  constructor(
    @Inject(PersonCardService.serviceName) private personCardService: PersonCardService,
    private route: ActivatedRoute,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      // const id = +params['id']; // (+) converts string 'id' to a number
      this.findCardById(params['id']);
    });
  }

  save(card: Card) {

  }

  openEdit(template: TemplateRef<any>) {
    this.modalEditRef = this.modalService.show(template);
    this.modalEditRef.content.title = '';
    //this.bsModalRef.content.list = list;
  }

  cancel(card: Card) {
    this.edit = false;
  }

  findCardById(id: string) {
    this.personCardService.find(id)
      .then((card) => {
        this.card = card;
      });
  }
}
