import { NgModule } from '@angular/core';
import { SharedModule } from './shared/shared.module';
import { SharedModule as AppSharedModule } from '../../shared/shared.module';
import { SharedModule as PersonSectionLocationSharedModule} from '../section-location/shared/shared.module';
import { CardViewComponent } from './card-view/card-view.component';
import { routing } from './card.routing';

@NgModule({
  imports: [
    SharedModule,
    AppSharedModule,
    PersonSectionLocationSharedModule,
    routing
  ],
  declarations: [
    CardViewComponent
  ]
})
export class CardModule { }
