import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonCardRowComponent } from './person-card-row.component';

describe('PersonCardRowComponent', () => {
  let component: PersonCardRowComponent;
  let fixture: ComponentFixture<PersonCardRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonCardRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonCardRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
