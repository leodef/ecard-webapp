import { Component, OnInit, Input, Output, EventEmitter, TemplateRef } from '@angular/core';
import { Card } from '../../../../shared/_models/card';
import { Router } from '@angular/router';
import { Location, PlatformLocation } from '@angular/common';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-person-card-row',
  templateUrl: './person-card-row.component.html',
  styleUrls: ['./person-card-row.component.css']
})
export class PersonCardRowComponent implements OnInit {

  @Input() card: Card;

  @Output() select: EventEmitter<Card> = new EventEmitter<Card>();

  @Output() delete: EventEmitter<Card> = new EventEmitter<Card>();

  modalRef: BsModalRef;

  get backgroundColor(): string {
    return this.card.color;
  }
  set backgroundColor(backgroundColor: string) { }

  elementType = 'url';

  get url(): string {
    const origin = (this.platformLocation as any).location.origin;
    const url = `${origin}/person/card/view/${this.card.code}`;
    return url;
  }

  constructor(
    private router: Router,
    private modalService: BsModalService,
    private platformLocation: PlatformLocation
  ) { }

  ngOnInit() {
  }


  selectClick() {
    this.select.emit(this.card);
  }
  deleteClick() {
    this.delete.emit(this.card);
  }

  openQRCode(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.modalRef.content.title = this.card.title;
  }
}
