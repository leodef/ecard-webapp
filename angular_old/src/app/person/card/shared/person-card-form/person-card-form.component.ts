import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Card } from '../../../../shared/_models/card';
import * as _ from 'lodash';

@Component({
  selector: 'app-person-card-form',
  templateUrl: './person-card-form.component.html',
  styleUrls: ['./person-card-form.component.css']
})
export class PersonCardFormComponent implements OnInit {
  _form: FormGroup;

  editCode: Boolean;

  @Input() card: Card;

  @Output() cardChange: EventEmitter<Card> = new EventEmitter<Card>();

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.card = this.card || new Card();
    this._form = this.formBuilder.group({
      title: [
        this.card.title,
        [
          Validators.required,
          Validators.maxLength(50)
        ]
      ]
    });
    this._form.valueChanges.subscribe(data => {
      this.emitChanges(data);
    });
  }

  emitChanges(data: any) {
    /*
        language: this.card.language, SelectOption
        color: this.card.color, InputColor
        traking: this.card.traking, toogleButton
        type: this.card.type, SectionOption ngIf='isNew'
        code
    */
    this.card = _.merge(this.card, data);
    this.cardChange.emit(this.card);
  }

  isNew(): Boolean {
    if (this.card._id == null) {
      return false;
    }
    return true;
  }
}
