import { Component, OnInit, Input } from '@angular/core';
import { Card } from '../../../../shared/_models/card';

@Component({
  selector: 'app-person-card-tracking',
  templateUrl: './person-card-tracking.component.html',
  styleUrls: ['./person-card-tracking.component.css']
})
export class PersonCardTrackingComponent implements OnInit {

  @Input() card: Card;

  constructor() { }

  get isTracking(): boolean {
    return this.card.traking === true;
  }


  ngOnInit() {
  }

}
