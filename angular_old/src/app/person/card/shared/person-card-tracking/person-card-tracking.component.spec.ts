import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonCardTrackingComponent } from './person-card-tracking.component';

describe('PersonCardTrackingComponent', () => {
  let component: PersonCardTrackingComponent;
  let fixture: ComponentFixture<PersonCardTrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonCardTrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonCardTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
