import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonCardInfoComponent } from './person-card-info.component';

describe('PersonCardInfoComponent', () => {
  let component: PersonCardInfoComponent;
  let fixture: ComponentFixture<PersonCardInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonCardInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonCardInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
