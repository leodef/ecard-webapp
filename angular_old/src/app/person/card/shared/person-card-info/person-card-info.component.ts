import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Card } from '../../../../shared/_models/card';

@Component({
  selector: 'app-person-card-info',
  templateUrl: './person-card-info.component.html',
  styleUrls: ['./person-card-info.component.css']
})
export class PersonCardInfoComponent implements OnInit {

  @Input() card: Card;

  constructor() { }

  ngOnInit() {
  }

}
