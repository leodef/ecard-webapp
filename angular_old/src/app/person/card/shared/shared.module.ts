import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonCardRowComponent } from './person-card-row/person-card-row.component';
import { PersonCardInfoComponent } from './person-card-info/person-card-info.component';
import { PersonCardEditComponent } from './person-card-edit/person-card-edit.component';
import { PersonCardFormComponent } from './person-card-form/person-card-form.component';
import { PersonCardTrackingComponent } from './person-card-tracking/person-card-tracking.component';
import { SharedModule as AppPersonSharedModule } from '../../shared/shared.module';



@NgModule({
  imports: [
    AppPersonSharedModule
  ],
  exports: [
    AppPersonSharedModule,
    PersonCardRowComponent,
    PersonCardInfoComponent,
    PersonCardEditComponent,
    PersonCardFormComponent,
    PersonCardTrackingComponent,
  ],
  declarations: [
    PersonCardRowComponent,
    PersonCardInfoComponent,
    PersonCardEditComponent,
    PersonCardFormComponent,
    PersonCardTrackingComponent
  ]
})
export class SharedModule { }
