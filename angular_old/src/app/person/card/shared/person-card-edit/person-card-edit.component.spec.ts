import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonCardEditComponent } from './person-card-edit.component';

describe('PersonCardEditComponent', () => {
  let component: PersonCardEditComponent;
  let fixture: ComponentFixture<PersonCardEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonCardEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonCardEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
