import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Card } from '../../../../shared/_models/card';

@Component({
  selector: 'app-person-card-edit',
  templateUrl: './person-card-edit.component.html',
  styleUrls: ['./person-card-edit.component.css']
})
export class PersonCardEditComponent implements OnInit {

  @Input() card: Card;

  editCode: Boolean;

  @Output() save: EventEmitter<Card> = new EventEmitter<Card>();

  @Output() cancel: EventEmitter<Card> = new EventEmitter<Card>();


  constructor() { }

  ngOnInit() {
  }

  saveCard() {
    this.save.emit(this.card);
  }

  cancelCard() {
    this.cancel.emit(this.card);
  }
}
