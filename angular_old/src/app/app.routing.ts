import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const routes: Routes = [
  { path: '', loadChildren: './person/person.module#PersonModule'},
  { path: 'person', loadChildren: './person/person.module#PersonModule'},
  { path: 'admin', loadChildren: './admin/admin.module#AdminModule'},
  // { path: '', redirectTo: 'user', pathMatch: 'full' },
  // { path: 'eager', component: EagerComponent },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
