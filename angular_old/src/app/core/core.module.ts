import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { HttpModule } from '@angular/http';
import { MessageService } from './_services/message.service';
import { SelectOptionService } from './_services/select-option.service';
import { AuthService } from './_services/auth.service';
import { ProgressBarService } from './_services/progress-bar.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from 'ngx-bootstrap';
import { ButtonsModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    HttpModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    ButtonsModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  declarations: [],
  providers: [
    { provide: MessageService.serviceName, useClass: MessageService },
    { provide: SelectOptionService.serviceName, useClass: SelectOptionService },
    { provide: AuthService.serviceName, useClass: AuthService },
    { provide: ProgressBarService.serviceName, useClass: ProgressBarService },
  ]
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [

      ]
    };
  }
}
