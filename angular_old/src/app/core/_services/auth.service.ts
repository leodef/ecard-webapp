import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { LoginDTO } from '../../shared/_dtos/account/login.dto';
import { SessionProfileDTO } from '../../shared/_dtos/account/session-profile.dto';

@Injectable()
export class AuthService {

  static serviceName = 'AuthService';

  constructor() { }

  private get authTokenHeaderKey(): string {
    return 'x-auth-token';
  }
  private get sessionProfileStorageKey(): string {
    return 'currentProfileKey';
  }
  private get sessionUsernameStorageKey(): string {
    return 'currentUser';
  }

  private get sessionAccountidStorageKey(): string {
    return 'currentAccountId';
  }
  private get sessionTokenStorageKey(): string {
    return 'currentToken';
  }

  get username(): String {
    return localStorage.getItem(this.sessionUsernameStorageKey) || '';
  }

  set username(username: String) {
    if (!username) {
      localStorage.removeItem(this.sessionUsernameStorageKey);
      return;
    }
    localStorage.setItem(
      this.sessionUsernameStorageKey,
      username.toString());
  }

  get accountId(): String {
    return localStorage.getItem(this.sessionAccountidStorageKey) || '';
  }

  set accountId(id: String) {
    if (!id) {
      localStorage.removeItem(this.sessionAccountidStorageKey);
      return;
    }
    localStorage.setItem(
      this.sessionAccountidStorageKey,
      id.toString());
  }


  get profile(): SessionProfileDTO {
    return JSON.parse(localStorage.getItem(this.sessionProfileStorageKey));
  }

  set profile(sessionProfileDTO: SessionProfileDTO) {
    if (!sessionProfileDTO) {
      localStorage.removeItem(this.sessionProfileStorageKey);
      return;
    }
    localStorage.setItem(
      this.sessionProfileStorageKey,
      JSON.stringify(sessionProfileDTO));
  }

  get token(): String {
    return localStorage.getItem(this.sessionTokenStorageKey);
  }

  set token(token: String) {
    if (!token) {
      localStorage.removeItem(this.sessionTokenStorageKey);
      return;
    }
    localStorage.setItem(
      this.sessionTokenStorageKey,
      token.toString());
  }

  get isAuthenticated(): boolean {
    if (this.token) {
      return true;
    }
    return false;
  }

  login(loginDTO: LoginDTO, sessionProfileDTO: SessionProfileDTO, token: String | Headers): void {
    this.accountId = sessionProfileDTO.account._id;
    this.profile = sessionProfileDTO;
    this.username = loginDTO.username;
    if (token instanceof Headers) {
      this.token = token.get(this.authTokenHeaderKey);
    } else {
      this.token = token.toString();
    }
  }

  logout(): void {
    this.profile = null;
    this.username = null;
    this.token = null;
  }

  setHeaders(headers: Headers): Headers {
    if (!headers) { headers = new Headers(); }
    if (this.isAuthenticated) {
      headers.append(this.authTokenHeaderKey, this.token.toString());
    }
    return headers;
  }


}
