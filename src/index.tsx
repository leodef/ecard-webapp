import React from 'react';
import ReactDOM from 'react-dom';
import { IntlProvider, addLocaleData } from 'react-intl';
import Cookie from 'js-cookie';
import fetch from 'isomorphic-fetch';
import './index.scss';
import App from './app/containers/App';
import * as serviceWorker from './serviceWorker';

const locale = Cookie.get('locale') || 'en';
fetch(`assets/i18n/${locale}.json`)
  .then((res: { status: number; json: () => void; }) => {
    if (res.status >= 400) {
      throw new Error('Bad response from server');
    }
    return res.json();
  })
  .then((localeData: any) => {
    addLocaleData(require(`react-intl/locale-data/${locale}`))

    ReactDOM.render(
      <IntlProvider locale={locale} messages={localeData}>
      <App />
      </IntlProvider>,
        document.getElementById('root'));

        
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
  });


