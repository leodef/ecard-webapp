import React, { Component, ReactNode } from 'react';
import { FormattedMessage, injectIntl, InjectedIntlProps  } from 'react-intl';
import {Container, Col, Card, Form, Row, Button} from 'react-bootstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';
import './Login.scss';
// import PropTypes from 'prop-types';

class Login extends Component<LoginProps, {}> {
  onSubmit(values: any, params: any) {
    setTimeout(() => {
      params.setSubmitting(false);
    }, 400);
  }
  get SignupSchema(){
    return Yup.object().shape({
      usernameOrEmail: Yup.string()
        .required('Required'),
      password: Yup.string()
        .required('Required')
    });
  }

  render() {
    const { intl } = this.props;
    const initialValues={ usernameOrEmail: '', password: '' };
    const onSubmit = this.onSubmit.bind(this);
    const SignupSchema = this.SignupSchema;
    return (
      <Container className="Login align-middle">
        <Row style={{height:40}}></Row>
        <Row>
          <Col></Col>
          <Col>
            <Card style={{ width: '18rem' }}>
              <Card.Body>
                <Card.Title>
                 <FormattedMessage id="public.Login.title" defaultMessage="Login"/>
                </Card.Title>
                <Formik
                  initialValues={initialValues}
                  validationSchema ={SignupSchema}
                  onSubmit={onSubmit}
                  render={props => (
                <Form onSubmit={props.handleSubmit}>
                  <Form.Group controlId="usernameOrEmail">
                    <Form.Label>
                      <FormattedMessage id="public.Login.inputUsernameOrEmail" defaultMessage="Username Or Password"/>
                      </Form.Label>
                    <Form.Control
                      type="text"
                      placeholder={intl.formatMessage({ id: 'public.Login.inputUsernameOrEmail' })}           
                      onChange={props.handleChange}
                      onBlur={props.handleBlur}
                      value={props.values.usernameOrEmail}
                      isInvalid={!!props.errors.usernameOrEmail}
                      />
                      <Form.Control.Feedback type="invalid">
                        {props.errors.usernameOrEmail}
                      </Form.Control.Feedback>
                  </Form.Group>
                  <Form.Group controlId="password">
                    <Form.Label>
                      <FormattedMessage id="public.Login.inputPassword" defaultMessage="Password"/>
                    </Form.Label>
                    <Form.Control
                    type="password"
                    placeholder={intl.formatMessage({ id: 'public.Login.inputPassword' })}
                    onChange={props.handleChange}
                    onBlur={props.handleBlur}
                    value={props.values.password}
                    isInvalid={!!props.errors.usernameOrEmail}
                    />
                    <Form.Control.Feedback type="invalid">
                      {props.errors.usernameOrEmail}
                    </Form.Control.Feedback>
                  </Form.Group>
                  <Button variant="primary" type="submit">
                    <FormattedMessage id="public.Login.buttonLogin" defaultMessage="Log In"/>
                  </Button>
                </Form>
              )}/>
              </Card.Body>
            </Card>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    );
  }
}

interface Action<T>
{
    (item: T): void;
}

interface LoginProps extends InjectedIntlProps{

}
export default injectIntl(Login);
