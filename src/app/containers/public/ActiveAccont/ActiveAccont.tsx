import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import logo from './logo.svg';
import './Home.scss';

class ActiveAccont extends Component {
  render() {
    return (
      <div className="ActiveAccont">
        <h1>
          <FormattedMessage id="public.Home.title" defaultMessage="Home"/>
        </h1>
      </div>
    );
  }
}

export default ActiveAccont;
