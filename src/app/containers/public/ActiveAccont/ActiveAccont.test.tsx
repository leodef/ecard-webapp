import React from 'react';
import ReactDOM from 'react-dom';
import ActiveAccont from './ActiveAccont';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ActiveAccont />, div);
  ReactDOM.unmountComponentAtNode(div);
});
