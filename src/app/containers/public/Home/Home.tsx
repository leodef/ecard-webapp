import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import './Home.scss';
import CardList from '../../../components/authenticated/Card/CardList/CardList';

class Home extends Component<HomeProps, HomeState> {
  constructor(props: any){
    super(props);
    const config: any = {
      card: {
        size: 4,
        permissions: { delete: true, field: true, edit: true, show: true, share: true}
      },
      field:{
        deep: 2,
        permissions: { delete: true, create: true, edit: true, show: true}
      }
    };
    const value: any = [{
      id:"001CARD",
      title: 'Vendedores',
      desc: 'Contato dos vendeores da empresa',
      field: {
        id:"001GRFI",
        type: 'group',
        title: 'Info Vendedor',
        name: 'seller',
        value: [
          {
            id:"002GRFI",
            type: 'group',
            title: 'Infomações Pessoais',
            name: 'personalFields',
            metaData: {orientation:'vertical', size: 4, hideTitle: true},
            value: [
              {
                id:"001TXFI",
                value:'Joao',
                title: 'Nome',
                name: 'nome',
                type: 'text'
              },
              {
                id:"001EMFI",
                value:'joao@gmail.com',
                title: 'Gmail',
                name: 'gmail',
                type: 'email'
              },
              {
                id:"001ADFI",
                value:' 2500 SE Technology Cir, Bentonville, AR 72712, EUA',
                title: 'Escritorio',
                name: 'address',
                type: 'location'
              },
              {
                id:"003GRFI",
                type: 'group',
                title: 'Sub informações',
                name: 'shirtInfo',
                metaData: {orientation:'vertical', size: 4, hideTitle: true},
                value: [
                  {
                    id:"002TXFI",
                    value:'Azul',
                    title: 'Cor camisa',
                    name: 'shirt_color',
                    type: 'text'
                  },
                  {
                    id:"003TXFI",
                    value:'42',
                    title: 'Medida camisa',
                    name: 'shirt_size',
                    type: 'text'
                  },
                ]
              },
            ]
          },
          {
            id:"004TXFI",
            value:'https://www.cobdoglaps.sa.edu.au/wp-content/uploads/2017/11/placeholder-profile-sq.jpg',
            title: 'Profile',
            name: 'profile',
            type: 'image'
          }
        ]
      }
    }];
    
    this.state = { config, value };
  }

  updateCard(){
    console.log('card update');
  }
  deleteCard(){
    console.log('card delete');
  }
  updateField(){
    console.log('card update');
  }
  deleteField(){
    console.log('card delete');
  }
  render() {
    const { value, config } = this.state;
    const params = {
      value,
      config,
      update: this.updateCard.bind(this),
      delete: this.deleteCard.bind(this),
      updateField: this.updateField.bind(this),
      deleteField: this.deleteField.bind(this)
    }
    return (
      <div className="Home">
        <h1>
          <FormattedMessage id="public.Home.title" defaultMessage="Home"/>
        </h1>
        <CardList {...params} />
      </div>
    );
  }
}

export interface HomeProps {

}

export interface HomeState {
  value?: any;
  config?: any;
}

export default Home;
