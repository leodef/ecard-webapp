import React from 'react';
import ReactDOM from 'react-dom';
import Public from './Public';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Public />, div);
  ReactDOM.unmountComponentAtNode(div);
});
