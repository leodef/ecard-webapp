import React, { Component } from 'react';
import { FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import {Container, Col, Card, Form, Row, Button} from 'react-bootstrap';
import * as Yup from 'yup';
import { Formik } from 'formik';
import './Signup.scss';

class Signup extends Component<SignupProps, {}> {

  onSubmit(values: any, params: any) {
    setTimeout(() => {
      params.setSubmitting(false);
    }, 400);
  }
  
  get SignupSchema(){
    return Yup.object().shape({
      phone: Yup.string()
        .required('Required'),
      username: Yup.string()
        .required('Required'),
      email: Yup.string()
        .email('Invalid email')
        .required('Required'),
      name: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
      birth: Yup.date()
        .required('Required'),
      password: Yup.string()
        .required('Required'),
      confirmPassword: Yup.string()
        .required('Required'),
      gender: Yup.string()
        .required('Required')
    });
  }
  render() {
    const { intl, value  } = this.props;
    const initialValues=
      value || { phone: '', username: '', email: '', name:'', birth: '', password: '', confirmPassword: '', gender: 'male' };
    //const validate = this.validate.bind(this);
    const onSubmit = this.onSubmit.bind(this);
    const SignupSchema = this.SignupSchema;
    return (
      <Container className="Signup align-middle">
        <Row style={{height:40}}></Row>
        <Row>
          <Col></Col>
          <Col md={8}>
            <Card>
              <Card.Body>
                <Card.Title>
                 <FormattedMessage id="public.Signup.title" defaultMessage="Signup"/>
                </Card.Title>
                <Formik
                  initialValues={initialValues}
                  validationSchema={SignupSchema} //validate={validate}
                  onSubmit={onSubmit}
                  render={props => (
                <Form>
                  <Form.Row>
                    <Form.Group as={Col} controlId="name">
                      <Form.Label>
                        <FormattedMessage id="public.Signup.inputName" defaultMessage="Name"/>
                        </Form.Label>
                      <Form.Control
                        type="text"
                        placeholder={intl.formatMessage({ id: 'public.Signup.inputName' })}
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        value={props.values.name}
                        isInvalid={!!props.errors.name}
                        />
                        <Form.Control.Feedback type="invalid">
                          {props.errors.name}
                        </Form.Control.Feedback>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group  as={Col} controlId="email">
                      <Form.Label>
                        <FormattedMessage id="public.Signup.inputEmail" defaultMessage="Email"/>
                      </Form.Label>
                      <Form.Control
                        type="email"
                        placeholder={intl.formatMessage({ id: 'public.Signup.inputEmail' })}
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        value={props.values.email}
                        isInvalid={!!props.errors.email}
                        />
                        <Form.Control.Feedback type="invalid">
                          {props.errors.email}
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} controlId="username">
                      <Form.Label>
                        <FormattedMessage id="public.Signup.inputUsername" defaultMessage="Username"/>
                      </Form.Label>
                      <Form.Control
                        type="text"
                        placeholder={intl.formatMessage({ id: 'public.Signup.inputUsername' })}
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        value={props.values.username}
                        isInvalid={!!props.errors.username}
                        />
                        <Form.Control.Feedback type="invalid">
                          {props.errors.username}
                        </Form.Control.Feedback>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group as={Col} controlId="password">
                      <Form.Label>
                        <FormattedMessage id="public.Signup.inputPassword" defaultMessage="Password"/>
                      </Form.Label>
                      <Form.Control
                        type="password"
                        placeholder={intl.formatMessage({ id: 'public.Signup.inputPassword' })}
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        value={props.values.password}
                        isInvalid={!!props.errors.password}
                        />
                        <Form.Control.Feedback type="invalid">
                          {props.errors.password}
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} controlId="confirmPassword">
                      <Form.Label>
                        <FormattedMessage id="public.Signup.inputConfirmPassword" defaultMessage="Confirm Password"/>
                      </Form.Label>
                      <Form.Control
                        type="password"
                        placeholder={intl.formatMessage({ id: 'public.Signup.inputConfirmPassword' })}
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        value={props.values.confirmPassword}
                        isInvalid={!!props.errors.confirmPassword}
                        />
                        <Form.Control.Feedback type="invalid">
                          {props.errors.confirmPassword}
                        </Form.Control.Feedback>
                    </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col} controlId="phone">
                    <Form.Label>
                      <FormattedMessage
                        id="public.Signup.inputPhone"
                        defaultMessage="Phone"/>
                    </Form.Label>
                    <Form.Control
                      type="text"
                      placeholder={intl.formatMessage({ id: 'public.Signup.inputPhone' })}
                      onChange={props.handleChange}
                      onBlur={props.handleBlur}
                      value={props.values.phone}
                      isInvalid={!!props.errors.phone}
                      />
                      <Form.Control.Feedback type="invalid">
                        {props.errors.phone}
                      </Form.Control.Feedback>
                  </Form.Group>

                  
                  <Form.Group as={Col} controlId="birth">
                    <Form.Label>
                      <FormattedMessage id="public.Signup.inputBirth" defaultMessage="Birth"/>
                    </Form.Label>
                    <Form.Control
                      type="date"
                      placeholder={intl.formatMessage({ id: 'public.Signup.inputBirth' })}
                      onChange={props.handleChange}
                      onBlur={props.handleBlur}
                      value={props.values.birth}
                      isInvalid={!!props.errors.birth}
                      />
                  </Form.Group>
                  <fieldset>
                    <Form.Group as={Col} controlId="gender">
                      <Form.Label>
                          {intl.formatMessage({ id: 'public.Signup.inputGender' })}
                      </Form.Label>
                      <Form.Check type="radio">
                        <Form.Check.Input
                          type="radio"
                          value="male"
                          name="gender" />
                        <Form.Check.Label>
                          {intl.formatMessage({ id: 'public.Signup.radioGenderMale' })}
                        </Form.Check.Label>
                      </Form.Check>

                      <Form.Check type="radio">
                        <Form.Check.Input
                          type="radio"
                          value="female"
                          name="gender"  />
                        <Form.Check.Label>
                          {intl.formatMessage({ id: 'public.Signup.radioGenderFemale' })}
                        </Form.Check.Label>
                      </Form.Check>
                    <Form.Control.Feedback type="invalid">
                          {props.errors.gender}
                        </Form.Control.Feedback>
                    </Form.Group>
                  </fieldset>
                  </Form.Row>
                  <Row>
                    <Col>
                      <Button variant="primary" type="submit">
                        <FormattedMessage id="public.Signup.buttonSignup" defaultMessage="Log In"/>
                      </Button>
                    </Col>
                  </Row>
                </Form>
              )} />
              </Card.Body>
            </Card>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    );
  }

   /*
    validate(values: any) {
      let errors: any = {};
      const { intl } = this.props;
      const required = intl.formatMessage({ id: 'share.forms.required' });
      if (!values.password) { errors.password = required; }
      if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
      ) {errors.email = 'Invalid email address'; }
      return errors;
    }
  */
}

interface SignupProps extends InjectedIntlProps{
  value: any;
}

export default injectIntl(Signup);
