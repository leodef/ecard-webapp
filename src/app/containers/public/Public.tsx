import React, { Component } from 'react';
import './Public.scss';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Header from '../../components/public/Header/Header';
import Home from './Home/Home';
import Login from './Login/Login';
import Signup from './Signup/Signup';
import Contacts from '../authenticated/Contacts/Contacts';
import MetaDataConfig from '../admin/MetaDataConfigManage/MetaDataConfigManage';


class Public extends Component {
  render() {
    return (
	
    <Router>
      <div className="Public">
        <Header />
        <Route exact path="/" component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/signup" component={Signup} />

        
        <Route path="/contacts" component={Contacts} />
        <Route path="/metadataconfig" component={MetaDataConfig} />

        
      </div>
    </Router>
    );
  }
}

export default Public;
