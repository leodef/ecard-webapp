import React, { Component } from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import './App.scss';
//import 'react-bootstrap-typeahead/css/Typeahead';
import Public from './public/Public';
import Authenticated from './authenticated/Authenticated';



library.add(fas, far);

class App extends Component<{}, AppState> {
  
  constructor(props: any) {
    super(props);
    this.state = {isAuth:true};
  }
  render() {
    const isAuth = this.state.isAuth;
    return (
      <div className="App">
        <Template isAuth={isAuth}/>
      </div>
    );
  }
}
function Template(props: any){
  if(props.isAuth){
    return <Public/>;
  } else {
    return <Authenticated/>
  }
}
class AppState {
  isAuth = false;
}
export default App;
