import React, { Component } from 'react';
import { Container, Row, Col, Breadcrumb } from 'react-bootstrap';
import CardFolder from '../../../components/authenticated/Card/CardFolder/CardFolder';

class Contacts extends Component<{}, ContactsState> {
  constructor(props: any){
    super(props);
    const root = {
      title: 'Root',
      type: 'folder',
      items: [
        {
          title: 'Jose',
          type: 'card',
          card: 'id_card_jose'
        },
        {
          title: 'Amigos',
          type: 'folder',
          items: [
            {
              title: 'Paulo',
              type: 'card',
              card: 'id_card_paulo'
            },
            {
              title: 'Alice',
              type: 'card',
              card: 'id_card_alice'
            }
          ]
        },
        {
          title: 'Restaurantes',
          type: 'folder',
          items: [
            {
              title: 'La Nonna',
              type: 'card',
              card: 'id_card_la_nonna'
            },
            {
              title: 'El Tarfo',
              type: 'card',
              card: 'id_card_el_tarfo'
            },
            {
              title: 'Tratoria',
              type: 'card',
              card: 'id_card_tratoria'
            },
            {
              title: 'Veganos',
              type: 'folder',
              items: [
                {
                  title: 'Sem graca',
                  type: 'card',
                  card: 'id_card_sem_graca'
                }
              ]
            }
          ]
        }
      ]
    };
    this.state = {
      root: root, breadcrumbs: [root], selected: root
    };
  }

  select(item: any){
    const {breadcrumbs} = this.state;
    if(item.type == 'folder'){
      breadcrumbs.push(item);
      this.setState({
        breadcrumbs: breadcrumbs,
        selected: item
      });
    }
  }

  back(item: any, index: number){
    const {breadcrumbs} = this.state;
    this.setState({
      breadcrumbs: breadcrumbs.slice(0, index + 1),
      selected: item
    });
  }

  renderBreadcrumbItem(){
    const {breadcrumbs} = this.state;
    const onClick = this.back.bind(this);
    const id = `contacts-breadcrumbs-id-${breadcrumbs.length}`;
    return breadcrumbs.map( (val, index) => {
      const params = {
          id,
          active: false,
          onClick: ()=>{}
        }
        if(index === breadcrumbs.length -1){
          params.active = true;
        } else {
          params.onClick = ()=>onClick(val, index);
        }
      return (
        <Breadcrumb.Item {...params} key={index} >
          {val.title}
        </Breadcrumb.Item>)
    })
  }
  render() {
    
    const {selected, breadcrumbs} = this.state;
    const renderBreadcrumbItem = this.renderBreadcrumbItem.bind(this);
    const config = {
      onClick: this.select.bind(this)
    }
    return (
      <Container>
      <Row>
        <Col sm="12">
        <Breadcrumb>
          {renderBreadcrumbItem()}
        </Breadcrumb>
        </Col>
      </Row>
      <Row className="justify-content-md-center">
        <Col sm="12">
          <CardFolder {...{config, value: selected, position: breadcrumbs.length}} />
        </Col>
      </Row>
      </Container>
    );
  }
}
interface ContactsState {
  root: any;
  breadcrumbs: Array<any>;
  selected: any;
}

export default Contacts;
