import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import logo from './logo.svg';
import './Dashboard.scss';

class Dashboard extends Component {
  render() {
    return (
      <div className="Home">
        <h1>
          <FormattedMessage id="public.Home.title" defaultMessage="Home"/>
        </h1>
      </div>
    );
  }
}

export default Dashboard;
