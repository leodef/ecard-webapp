import React from 'react';
import ReactDOM from 'react-dom';
import MetaDataConfigManage from './MetaDataConfigManage';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MetaDataConfigManage {...{value:{}, config: {}}}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
