import React, { Component } from 'react';
import { InjectedIntlProps, injectIntl, FormattedMessage } from 'react-intl';
import { Card, Button, Container, Col, Form, Row } from 'react-bootstrap';
import { Typeahead } from 'react-bootstrap-typeahead';
const Accordion = require('react-bootstrap/Accordion');
import 'react-bootstrap-typeahead/css/Typeahead.css';
import './MetaDataConfigManage.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import MetaDataConfigList from '../../../components/admin/MetaDataConfig/MetaDataConfigList/MetaDataConfigList';

class MetaDataConfigManage extends Component<MetaDataConfigManageProps, MetaDataConfigManageState> {

  constructor(props: MetaDataConfigManageProps){
    super(props);
    this.state = this.defaultState();
  }

  defaultState(): MetaDataConfigManageState{
  const targets = [
      {id: "34", title:"Group", test: "group-item"}, {id: "12", title:"Field", test: "field-item"} 
  ];
  const filter = {targets: [], text: ''} as Filter;
  const result: Array<any> = new Array<any>();
  const config = {

  };
  return {
      targets,
      filter,
      result,
      config
    };
  }


  render(){
    let { targets } = this.state;
    targets = targets || [];
    console.log(targets);
    return (
      <Accordion defaultActiveKey="1">
      <Card>
        <Card.Header>
          <Accordion.Toggle as={Button} variant="link" eventKey="0">
            Targets
          </Accordion.Toggle>
        </Card.Header>
        <Accordion.Collapse eventKey="0">
          <Card.Body className="typeaheadCard">        
        
          {this.renderTargets()}
          </Card.Body>
        </Accordion.Collapse>
      </Card>
      <Card>
        <Card.Header>
          <Accordion.Toggle as={Button} variant="link" eventKey="1">
            Search
          </Accordion.Toggle>
        </Card.Header>
        <Accordion.Collapse eventKey="1">
          <Card.Body  className="typeaheadCard"> 
           {this.renderFilter()}
          </Card.Body>
        </Accordion.Collapse>
      </Card>
    </Accordion>);
  }

  renderTargets(){
    let { targets } = this.state;
    return (
      <Typeahead
        id="targets"
        multiple
        allowNew
        labelKey="title"
        newSelectionPrefix="Novo item: "
        onChange={this.changeTargets.bind(this)}
        selected={targets}// Array of objects or strings
        options={[]}
      />);
  }

  changeTargets(targets: Array<any>):void{
    console.log('changeTargets ->'+ JSON.stringify(targets));
    this.setState({targets: targets.map(target => {
      if(target.customOption){
        const {customOption, ...val} = target;
        return val;
      }
      return target;
    })});

  }

  renderFilter(){
    let { intl } = this.props;
    let { targets, filter } = this.state;
    targets = targets || [];
    return(
      <Container>
        <Row>
          <Col>
            <Typeahead
              id="filter-targets"
              multiple
              labelKey="title"
              newSelectionPrefix="Novo item: "
              options={targets}// Array of objects or strings
              selected={filter.targets}
              onChange={(val: any) => this.changeFilter({targets: val as Array<any>})}
            />
        </Col>
      </Row>
      <Row>
        <Form.Group as={Col} controlId="name">
              <Form.Label>
                <FormattedMessage id="public.Signup.inputName" defaultMessage="Name"/>
                </Form.Label>
              <Form.Control
                type="text"
                defaultValue={filter.text}
                onChange={(val: any) => this.changeFilter({text: val.toString()})}
                placeholder={intl.formatMessage({ id: 'public.Signup.inputName' })}
                />
                <Form.Control.Feedback type="invalid">
                </Form.Control.Feedback>
        </Form.Group>
      </Row>
      <Row>
        <Col>
          <Button
            onClick={this.execFilter.bind(this)}
            variant="primary"
            type="submit">
            <FontAwesomeIcon
              icon="filter"
              className="align-top ml-2" />
        </Button>
        </Col>
      </Row>
    </Container>
    )
  }

  changeFilter(val: any){
    this.setState(state => ({
      filter: {
        ...state.filter,
        ... val
      } as Filter
    }))
  }

  execFilter(){
    const { filter } = this.state;

  }

  renderResult(){
    const { result, config } = this.state;
    const params = {
      value: result,
      config
    }
    return (<MetaDataConfigList {...params}></MetaDataConfigList>); 
  }
}

interface MetaDataConfigManageProps extends InjectedIntlProps{
}

interface MetaDataConfigManageState {
  targets?: Array<any>;
  filter: Filter;
  config: any;
  result: Array<any>;
}
export class Filter {
  text: any;
  targets: Array<any> = [];
}
export default injectIntl(MetaDataConfigManage);
