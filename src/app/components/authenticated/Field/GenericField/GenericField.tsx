import React, { Component, FormEvent } from 'react';
import {  injectIntl, InjectedIntlProps } from 'react-intl';
import { Col, Form, Row, Container } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './GenericField.scss';


class GenericField extends Component<GenericFieldProps, GenericFieldState> {
	
	constructor(props: GenericFieldProps) {
    super(props);
    this.state = this.defaultState();
	}

  defaultState(): GenericFieldState {
    const props = this.props;
    const { value, config, action, deepPosition, update, open } = props;
    const deleteF = props.delete;
    return {
      value,
      config,
      action : action || 'SHOW',
      deepPosition,
      update,
      delete: deleteF,
      open 
    };
  }

  edit(){
    this.setState({action: 'EDIT'}); 
  }

  show(){
    this.setState({action: 'SHOW'}); 
  }
  
  delete(){
    const { value } = this.state;
    const deleteF = this.state.delete;
    if(deleteF){
      deleteF(value);
    }
  }

  update(){
    const { update, value } = this.state;
    if(update){
      update(value);
    }
  }

  onPointerEnter(event: any){
    this.setState({mouseOn: true});
  }

  onPointerLeave(event: any){
    this.setState({mouseOn: false});
  }

  render() {
    const {  action } = this.state;
    switch(action){
      case 'EDIT':
        return this.renderEdit();
      case 'SHOW':
        return this.renderShow();
      default:
          return this.renderShow();
      }
  }

  updateValue(obj: any){
    this.setState(state => ({
      value: {
        ...state.value,
        ...obj
      }
    }));
  }

  renderEdit(){
    const { value, config, mouseOn } = this.state;
    const permissions = config.field.permissions;
    if((
      permissions &&
      !permissions.edit
      )){return (<span>Unauthorized</span>);}
    return (
      <Container
      onPointerEnter={this.onPointerEnter.bind(this)}
      onPointerLeave={this.onPointerLeave.bind(this)}>
    <Row>
      <Col>
        <Form.Group  controlId={value.name}>
          <Form.Label>
            {value.title || value.name}
          </Form.Label>
          <Form.Control
            type="text"
            name={value.name}
            placeholder={value.title || value.name}
            defaultValue={value.value}
            onChange={(val: any)=> this.updateValue({value: val})}
            />
        </Form.Group>
      </Col>
      <Col sm={1}>
      {
        !mouseOn? null: (<Row>
          {!permissions.show ? null : <Col>
            <span onClick={this.show.bind(this)}>
              <FontAwesomeIcon
                icon="times"
                className="align-top ml-2" />
            </span> </Col>
          }
          {!permissions.edit ? null : <Col>
            <span onClick={this.update.bind(this)}>
              <FontAwesomeIcon
                icon="save"
                className="align-top ml-2" />
            </span> </Col>
          }
          { !permissions.delete ? null:
            (<span onClick={this.delete.bind(this)}>
              <FontAwesomeIcon
                icon="trash"
                className="align-top ml-2" />
            </span>)
          }
          </Row>)}
      </Col>
    </Row>
    </Container>);
  }

  renderShow(){
    const { value, config, mouseOn } = this.state;
    const permissions = config.field.permissions;
    if((
      permissions &&
      !permissions.show
      )){return (<span>Unauthorized</span>);}
    return (//shadow 
    <Container
      onPointerEnter={this.onPointerEnter.bind(this)}
      onPointerLeave={this.onPointerLeave.bind(this)}>
    <Row>
      <Col>
        <span>
          <strong className="mr-2">
            {value.title || value.name}
          </strong>
          {value.value}
        </span>
      </Col>
      
      <Col sm={1}>
      {
        !mouseOn? null: (<Row>
          {!permissions.edit ? null :
            (<span onClick={this.edit.bind(this)}>
              <FontAwesomeIcon
                icon="edit"
                className="align-top ml-2" />
            </span>)
          }
          { !permissions.delete ? null:
            (<span onClick={this.delete.bind(this)}>
              <FontAwesomeIcon
                icon="trash"
                className="align-top ml-2" />
            </span>)
          }
        </Row>)}
      </Col>
    </Row>
    </Container>);
  }
}
export interface GenericFieldProps extends InjectedIntlProps{
  value: any; // Input
  config: any;
  action?: string;
  deepPosition?: number;
  update?: Function,
  delete?: Function,
  open?: Function
}
export interface GenericFieldState{
  value?: any;
  config?: any;
  action?: string;
  deepPosition?: number;
  update?: Function,
  delete?: Function,
  open?: Function
  mouseOn?: boolean;
}

export default GenericField;
