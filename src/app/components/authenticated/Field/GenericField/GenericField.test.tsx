import React from 'react';
import ReactDOM from 'react-dom';
import GenericField from './GenericField';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<GenericField  {...{value:{}, config:{} , 
   }}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
