import React from 'react';
import ReactDOM from 'react-dom';
import GroupField from './GroupField';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<GroupField {...{value:{}}}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
