import React, { Component } from 'react';
import { injectIntl } from 'react-intl';
import { Form, Container, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './GroupField.scss';
import Field, { FieldProps } from '../Field';
import GenericField from '../GenericField/GenericField';

class GroupField extends GenericField {
	constructor(props: FieldProps) {
    super(props);
  }

  // render
  render() {
    const { value } = this.state;
    const resume = this.resume();
    const open = this.open.bind(this);
    let title = (value.title || value.name);
    if(value.metaData && value.metaData.hideTitle && !resume){
      title = '';
    }
    return (
      <Container>
        <Row
          onClick={open}
          className="justify-content-md-center">
          {title}
          {resume ?  <FontAwesomeIcon
           icon="expand"
           className="d-inline-block align-top ml-2"
           /> : null}
        </Row>
      {resume ? null :(<Row>
        {value.value.map(this.renderCol.bind(this))}
      </Row>)}
      </Container>);
  }

  renderCol(child: any, index: number){
    const state = this.state;
    const { config, deepPosition, open, update, action } = state;
    const deleteF = state.delete
    const childPosition = (deepPosition || 0) + 1;
    const id = `group-field-item-${index}`;
    const sizes = this.getColSizes(child);
    const params = {
      value: child,
      config,
      resume: true,
      deepPosition: childPosition,
      open, update, delete:deleteF, action
    };
    return (
      <Col md="4" id={id} {...sizes} key={index}>
        <Field {...params} key={child.id}/>
      </Col>);
  }

  // callback
  open(){
    const { open, value } = this.state;
    if(open){
      open(value);
    }
  }

  // utils
  resume(){
    const { deepPosition, config } = this.props;
    let deep = 0;
    if(config.field && config.field.deep){
      deep = config.field.deep;
    }
    return  ((deepPosition || 0) >= deep);
  }

  getColSizes(field: any, size?: string): any{
    const { value } = this.props;
    if(size){
      let order = field.order || 0;
      let span = 12;
      const metaData = field.metaData || {};
      const fieldSize = metaData.size || 0; // 0 - 4
      if(
        value.metaData &&
        value.metaData.orientation &&
        value.metaData.orientation.toLowerCase() === 'vertical'){
        return {order, span};
      }
      if(Number.isNaN(fieldSize)){
        span = fieldSize;
        return { order, span };
      }
      switch(size){
        case 'xl':
          span = fieldSize + 4;
        break;
        case 'lg':
          span = fieldSize + 4;
        break;
        case 'md':
          span = fieldSize + 6;
        break;
        case 'sm':
          span = fieldSize + 8;
        break;
        case 'xs':
          span = fieldSize + 10;
        break;
      }
      if(span > 12){ span = 12;}
      return {order, span};
    } else {
      return {
        xl: this.getColSizes(field, 'xl'),
        lg: this.getColSizes(field, 'lg'),
        md: this.getColSizes(field, 'md'),
        sm: this.getColSizes(field, 'sm'),
        xs: this.getColSizes(field, 'xs'),
      }
    }
  }
}

export default injectIntl(GroupField);
