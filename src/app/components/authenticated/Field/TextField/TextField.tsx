import GenericField from '../GenericField/GenericField';
import './TextField.scss';
import { injectIntl } from 'react-intl';


class TextField extends GenericField{
	
}

export default injectIntl(TextField);
