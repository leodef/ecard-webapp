import './EmailField.scss';
import GenericField from '../GenericField/GenericField';
import { injectIntl } from 'react-intl';


class EmailField  extends GenericField{
	
}

export default injectIntl(EmailField);
