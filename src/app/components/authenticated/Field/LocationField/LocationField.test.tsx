import React from 'react';
import ReactDOM from 'react-dom';
import LocationField from './LocationField';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<LocationField  {...{value:{}}}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
