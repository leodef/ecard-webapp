
import './LocationField.scss';
import GenericField from '../GenericField/GenericField';
import { injectIntl } from 'react-intl';


class LocationField extends GenericField{
	
}

export default injectIntl(LocationField);
