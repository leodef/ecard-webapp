import React, { Component } from 'react';
import {  injectIntl } from 'react-intl';
import {Form, Figure} from 'react-bootstrap';
import './ImageField.scss';
import { FieldProps } from '../Field';
import GenericField from '../GenericField/GenericField';


class ImageField extends GenericField {
	constructor(props: FieldProps) {
    super(props);
	}
 
  renderEdit(){
    const { value, config } = this.props;
    const changeValue = this.changeValue.bind(this);
    if((
      config.field.permissions &&
      !config.field.permissions.edit
      )){return (<span>Unauthorized</span>);}
    return (
      <Form.Group  controlId={value.name}>
        <Form.Control
          type="text"
          name={value.name}
          placeholder={value.title || value.name}
          defaultValue={value.value}
          onChange={changeValue} />
      </Form.Group>);
  }
  renderShow(){
    const { value, config } = this.props;
    if((
    
      config.field.permissions &&
      !config.field.permissions.show
      )){return (<span>Unauthorized</span>);}
    return (
      <Figure>
        <Figure.Image
          // width={171}  height={180}
          alt={value.title || value.name}
          src={value.value} />
        <Figure.Caption> {value.title || value.name} </Figure.Caption>
      </Figure>);
  }
  changeValue(event: any){
    const { value, config } = this.props;
    value.value = event.target.value;
    if(config.field && config.field.callback && config.field.callback.update){
      config.field.callback.update(value);
    }
  }
  
}


export default injectIntl(ImageField);
