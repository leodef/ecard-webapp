import React from 'react';
import ReactDOM from 'react-dom';
import ImageField from './ImageField';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ImageField  {...{value:{}}}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
