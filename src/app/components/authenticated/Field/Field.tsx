import React, { Component } from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import  * as _ from 'lodash';
import * as comp from './GroupField/GroupField';
import './Field.scss';
import { string } from 'yup';


class Field extends Component<FieldProps, FieldState> {
 
	constructor(props: FieldProps) {
    super(props);
    this.state = {fieldConn: new FieldConn()};
  }

    // after the initial render, wait for module to load
  async componentDidMount() {
    const props = this.props;
    let { fieldConn } = this.state;
    fieldConn = fieldConn || {};
    const { value } = props;/* type: text, value: Rua X, name: address*/
    let fieldComponent = null;
    if(!value || !value.type){
      fieldConn.error = true;
      this.setState({fieldConn});
      return;
    }
    const fieldUrl = `./${_.capitalize(value.type)}Field/${_.capitalize(value.type)}Field`;
    fieldComponent = await require(`./${_.capitalize(value.type)}Field/${_.capitalize(value.type)}Field`).default
    let error = false;
    if(! fieldComponent){error = true;}
    this.setState({fieldConn: { fieldComponent, error, fieldUrl }});
  }

  render() {
    const props = this.props;
    let {fieldConn} = this.state;
    fieldConn = fieldConn || {};
    if(fieldConn.fieldComponent){
      return React.createElement(fieldConn.fieldComponent, props);
    } else {
      if(fieldConn.error){
        return (<div><span>Field Not Working</span></div>);
      }
      return (<div><span>Loading Field</span></div>);
    }
    
  }


}
export interface FieldProps extends InjectedIntlProps{
  value: any; // Input
  config: any;
  deepPosition?: number;
  action?: string;
  update?: Function,
  delete?: Function,
  open?: Function
}
export interface FieldState{
  fieldConn?: FieldConn;
  mouseOn?: boolean;
  action?: string;
  value?: any;
  config?: any;
}

export class FieldConn {
  fieldComponent?: any;
    fieldUrl?: string;
    error?:boolean;
 }

export default injectIntl(Field);
