import './PhoneField.scss';
import GenericField from '../GenericField/GenericField';
import { injectIntl } from 'react-intl';


class PhoneField extends GenericField{
	
}

export default injectIntl(PhoneField);

