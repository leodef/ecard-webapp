import React, { Component } from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import { Container, Row, Col, Badge } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class CardFolder extends Component<CardFolderProps, {}> {
	constructor(props: CardFolderProps) {
    super(props);
	}

  render() {
    const { value } = this.props;
    return (
      <Container>
      <Row className="justify-content-md-center">
        {value.items.map(this.renderIcon.bind(this))}
      </Row>
      </Container>);
  }

  renderIcon(item: any, index: number){
    const { position } = this.props;
    const icon = this.getIcon(item);
    const id = `card-folder-item-${position}-${index}`;
    const onClick = () =>{ this.onIconClick(item);};
    return (
    <Col sm="auto" id={id} key={index} onClick={onClick}>
		    <FontAwesomeIcon
            key={index}
            size="lg"
            icon={icon}
            className="align-top ml-2 FolderIcon"
          />
          <p key={id}>
            {item.title}
            {item.type !== 'folder' ? null :(
            <Badge variant="light">
              {(item.items || []).length}
            </Badge>)
          }
          </p>
    </Col>);
  }
  onIconClick(item: {type: string}){
    const {value, config} = this.props;
    if(config.onClick){config.onClick(item, value);}
  }
  getIcon(item: {type: string}):any {
    if(item.type == 'folder'){
        return ['far', 'folder'];
    } else if (item.type == 'card'){
        return ['fas','address-card'];
    } else {
        return 'question';
    }
  }

}
interface CardFolderProps extends InjectedIntlProps{
  value: any;
  config: any;
  position: number;
}

export default injectIntl(CardFolder);
