import React from 'react';
import ReactDOM from 'react-dom';
import CardFolder from './CardFolder';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CardFolder {...{value: {}, config: {}, position: 0}} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
