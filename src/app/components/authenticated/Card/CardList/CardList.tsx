import React, { Component } from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import { Container, Row, Col } from 'react-bootstrap';
import Card from '../Card';


class CardList extends Component<CardListProps, {}> {
	constructor(props: CardListProps) {
    super(props);
	}

  render() {
    const {  value } = this.props;
    return (
      <Container>
      <Row className="justify-content-md-center">
        {value.map(this.renderCol.bind(this))}
      </Row>
      </Container>);
  }

  renderCol(card: any, index: number){
    const props = this.props;
    const id = `card-list-item-${index}`;
    const sizes = this.getColSizes(card);
    return (
    <Col key={index} id={id} {...sizes}>
      <Card {...props} value={card} key={card.id}/>
    </Col>);
  }

  getColSizes(card: any, size?: string ): any{
    const { value, config } = this.props;
    const configCard = config.card;
    let vertical = false;
      if(configCard){
        if(
          configCard.orientation &&
          configCard.orientation.toLowerCase() === 'vertical'){
            vertical = true;
        }
    }
    if(size){
      let order = card.order || 0;
      let span = 12;
      let cardSize = 0;
      
      if(configCard){
        if(vertical) {
          return {order, span};
        }
        if(
          configCard.size ){
          cardSize = configCard.size || 0;
        }
        if(Number.isNaN(cardSize)){
          span = cardSize;
          return {order, span};
        }
      }
      switch(size){
        case 'xl':
          span = cardSize + 4;
        break;
        case 'lg':
          span = cardSize + 4;
        break;
        case 'md':
          span = cardSize + 6;
        break;
        case 'sm':
          span = cardSize + 8;
        break;
        case 'xs':
          span = cardSize + 10;
        break;
      }
      if(span > 12){ span = 12;}
      return {order, span};
    } else {
      return {
        xl: this.getColSizes(card, 'xl'),
        lg: this.getColSizes(card, 'lg'),
        md: this.getColSizes(card, 'md'),
        sm: this.getColSizes(card, 'sm'),
        xs: this.getColSizes(card, 'xs'),
      }
    }
  }
}
interface CardListProps extends InjectedIntlProps{
  value: any;
  config: any;
}

export default injectIntl(CardList);
