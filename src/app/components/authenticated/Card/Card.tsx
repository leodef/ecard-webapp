import React, { Component } from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import { Card as BootstrapCard, Breadcrumb, Row, Container, Col, InputGroup, Form, Tooltip, Overlay, OverlayTrigger } from 'react-bootstrap';
//import * as  QRCode from 'qrcode.react';
const QRCode = require('qrcode.react');
import Field from '../Field/Field';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


class Card extends Component<CardProps, CardState> {
  
  cardIdInput?: any;
  qrCode?: any;
  copyToClipboardButton?: any;

	constructor(props: CardProps) {
    super(props);
    this.state = this.defaultState();
	}

  defaultState(): CardState{
    const props = this.props;
    let { value, config, action, update, updateField, deleteField } = props;
    const dFunc = props.delete;

    // config
    config.field.deep = config.field.deep || value.deep;
    action = action || 'SHOW';
    // fieldBreadcrumb
    let fieldBreadcrumb = null;
    if(this.showFields()){
      let root = value.field;
      fieldBreadcrumb = {
        breadcrumbs: [root],
        selected: root,
        root
      };
    }
    return {
      value,
      config,
      update,
      updateField,
      deleteField,
      delete: dFunc,
      action,
      fieldBreadcrumb,
      mouseOn: false
    };
  }

  // callbacks
  openField(selected: any){
    if(!this.showFields()){return false;}
    const {fieldBreadcrumb} = this.state;
    if(selected.type == 'group'){
      const breadcrumbs = fieldBreadcrumb.breadcrumbs;
      const root = fieldBreadcrumb.root;
      breadcrumbs.push(selected);
      this.setState({fieldBreadcrumb:{
        breadcrumbs,
        selected,
        root
      }});
    }
  }

  back(item: any, index: number){
    const { fieldBreadcrumb} = this.state;
    this.setState({fieldBreadcrumb:{
      breadcrumbs: fieldBreadcrumb.breadcrumbs.slice(0, index + 1),
      selected: item
    }});
  }

  edit(){
    this.setState({action: 'EDIT'}); 
  }

  show(){
    this.setState({action: 'SHOW'}); 
  }

  export(){
    this.setState({action: 'EXPORT'}); 
  }

  delete(){
    const { value } = this.state;
    const deleteF = this.state.delete;
    if(deleteF){
      deleteF(value);
    }
  }

  update(){
    const { value, update } = this.state;
    if(update){
      update(value);
    }
  }

  updateField(field: any){
    const { updateField } = this.state;
    if(updateField){
      updateField(field);
    }
  }

  deleteField(field: any){
    const { deleteField } = this.state;
    if(deleteField){
      deleteField(field);
    }
  }

  onPointerEnter(event: any){
    this.setState({mouseOn: true});
  }

  onPointerLeave(event: any){
    this.setState({mouseOn: false});
  }

  render(){
    const { config, action } = this.state;
    switch(action){
      case 'EDIT':
        return this.renderEdit();
      case 'EXPORT':
        return this.renderExport();
      case 'SHOW':
        return this.renderShow();
      default:
        return this.renderShow();

    }
  }

  // render
  renderEdit(){
    const { value, config, mouseOn } = this.state;
    const permissions = config.card.permissions;
    if((
      permissions &&
      !permissions.edit
      )){return (<span>Unauthorized</span>);}
    return (
      <BootstrapCard
        onPointerEnter={this.onPointerEnter.bind(this)}
        onPointerLeave={this.onPointerLeave.bind(this)}>
        <BootstrapCard.Body>
          <BootstrapCard.Title as={Row}>
            <Col></Col>
            <Col>
              {value.title}
            </Col>
            <Col>
            { !mouseOn ? null: (<Row>
              { !permissions.edit ? null :
                (<span onClick={this.update.bind(this)}>
                  <FontAwesomeIcon
                    icon="save"
                    className="align-top ml-2" />
                </span>)
              }
              { !permissions.share ? null :
                (<span onClick={this.export.bind(this)}>
                  <FontAwesomeIcon
                    icon="share"
                    className="align-top ml-2" />
                </span>)
              }
              { !permissions.show ? null :
                (<span onClick={this.show.bind(this)}>
                  <FontAwesomeIcon
                    icon="times"
                    className="align-top ml-2" />
                </span>)
              }
              { !permissions.delete? null:
                  (<span onClick={this.delete.bind(this)}>
                    <FontAwesomeIcon
                      icon="trash"
                      className="align-top ml-2" />
                  </span>)
              }
              </Row>)}
            </Col>
          </BootstrapCard.Title>
          <BootstrapCard.Subtitle className="mb-2 text-muted">
            {value.desc}
          </BootstrapCard.Subtitle>
            {
              permissions.field ?
              this.renderField():
              null
            }
        </BootstrapCard.Body>
      </BootstrapCard>);
  }

  renderShow() {
    const { value, config, mouseOn } = this.state;
    const permissions = config.card.permissions;
    if((
      permissions &&
      !permissions.show
      )){return (<span>Unauthorized</span>);}
    return (
		  <BootstrapCard
        onPointerEnter={this.onPointerEnter.bind(this)}
        onPointerLeave={this.onPointerLeave.bind(this)}>
        <BootstrapCard.Body>
          <BootstrapCard.Title as={Row}>
            <Col></Col>
            <Col>
              {value.title}
            </Col>
            <Col>
            { !mouseOn ? null:(<Row>
                { !permissions.share? null:
                  (<span onClick={this.export.bind(this)}>
                    <FontAwesomeIcon
                      icon="share"
                      className="align-top ml-2" />
                  </span>)
                }
                { !permissions.edit ? null:
                  (<span onClick={this.edit.bind(this)}>
                    <FontAwesomeIcon
                      icon="edit"
                      className="align-top ml-2" />
                  </span>)
                }
                { !permissions.delete ? null:
                    (<span onClick={this.delete.bind(this)}>
                      <FontAwesomeIcon
                        icon="trash"
                        className="align-top ml-2" />
                    </span>)
                }
                </Row>)
              }
              </Col>
            
          </BootstrapCard.Title>
          <BootstrapCard.Subtitle className="mb-2 text-muted">
            {value.desc}
          </BootstrapCard.Subtitle>
          {
            this.showFields() ?
            this.renderField():
            null
          }
        </BootstrapCard.Body>
      </BootstrapCard>
    );
  }

  renderField(){
    const { config, fieldBreadcrumb, action} = this.state;
    const { selected } = fieldBreadcrumb;
    const params = {
      config,
      action,
      value: selected,
      deepPosition: 0,
      update: this.updateField.bind(this),
      delete: this.deleteField.bind(this),
      open: this.openField.bind(this)
    }
    return (
      <Container>
      <Row>
        <Col>
        <Breadcrumb>
          {this.renderBreadcrumbItem()}
        </Breadcrumb>
        </Col>
      </Row>
      <Row className="justify-content-md-center">
        <Col>
        <Field {...params} key={selected.id}/>
        </Col>
      </Row>
      </Container>);
  }

  renderExport(){
    const { value, showCopyToClipboardTooltip, mouseOn } = this.state;
    const copyToClipboard = this.copyToClipboard.bind(this);
    return (
      <>
      <BootstrapCard
        onPointerEnter={this.onPointerEnter.bind(this)}
        onPointerLeave={this.onPointerLeave.bind(this)}>
      <BootstrapCard.Body>
         <BootstrapCard.Title as={Row}>
            <Col></Col>
            <Col>
              {value.title}
            </Col>
            <Col>
              { !mouseOn ? null: (<Row>
                  <span onClick={this.show.bind(this)}>
                    <FontAwesomeIcon
                      icon="times"
                      className="align-top ml-2" />
                  </span>
                </Row>)}
              </Col>
          </BootstrapCard.Title>
          <BootstrapCard.Subtitle className="mb-2 text-muted">
            {value.desc}
          </BootstrapCard.Subtitle>
        
      <Container>
        <Form as={Row} className="justify-content-md-center">
          <Col></Col>
        <Form.Group controlId="id" as={Col} >
          <Form.Label>
            ID
          </Form.Label>
          <InputGroup>
            <Form.Control
              type="text"
              //plaintext
              readOnly
              defaultValue={value.id}
              onBlur={(env: any)=>{this.hideCopyToClipboardTooltip();}}
              ref={(ref: any)=>{this.cardIdInput = ref}} />
              <InputGroup.Prepend>
                <InputGroup.Text
                onClick={copyToClipboard} >
                <FontAwesomeIcon
                  icon="copy"
                  className="align-top ml-2"
                />
                </InputGroup.Text>
              </InputGroup.Prepend>
          </InputGroup>
        </Form.Group>
        <Col></Col>
        </Form>
        <Row>
        <Col>
        <QRCode
          value={value.id}
          size={128}
          bgColor={"#FFFFFF"}
          fgColor={"#000000"}
          level={"L"}
          includeMargin={false}
          renderAs={"svg"}
          ref= { this.qrCode}
         /* img={
            {"src": "./logo.png",
            "top": 50, "left": 50, "width": 20, "height": 20}
          }
        */
        />
        </Col>
        </Row>
        </Container>
      </BootstrapCard.Body>
      </BootstrapCard>
       <Overlay
        key="copy-to-clipboard-overlay" 
        target={this.cardIdInput}
        show={showCopyToClipboardTooltip}
        placement="right">
       {(props: any) => 
         <Tooltip id="copy-to-clipboard-tooltip"  {...props} show={props.show.toString()}>
           Copied to Clipboard
         </Tooltip>
        }
     </Overlay>
   </>
    );
  }

  hideCopyToClipboardTooltip(){
    this.setState({showCopyToClipboardTooltip: false});
  }

  downloadQRCode(){
    const qrCode = this.qrCode;
    qrCode.download(QRCode.png);
  }

  copyToClipboard(){
    const cardIdInput = this.cardIdInput;
    if(cardIdInput){
      cardIdInput.select();
      document.execCommand('copy');
      // This is just personal preference.
      // I prefer to not show the the whole text area selected.
      cardIdInput.focus();
      this.setState({showCopyToClipboardTooltip: true});
    }
  }

  // breadcrump
  renderBreadcrumbItem(){
    const { fieldBreadcrumb } = this.state;
    const { breadcrumbs } = fieldBreadcrumb;
    const back = this.back.bind(this);
    const id = `card-breadcrumbs-id-${breadcrumbs.length}`;
    return breadcrumbs.map( (val: any, index: number) => {
      const params = {
          id,
          active: false,
          onClick: ()=>{}
        }
        if(index === breadcrumbs.length -1){
          params.active = true;
        } else {
          params.onClick = ()=>back(val, index);
        }
      return (
        <Breadcrumb.Item {...params} key={index} >
          {val.title}
        </Breadcrumb.Item>);
    })
  }

  // utils
  showFields(){
    const { value, config } = this.state || this.props;
    return (
      value.field &&
      config.card &&
      config.card.permissions &&
      config.card.permissions.field);
  }
}

interface CardProps extends InjectedIntlProps{
  value: any,
  config: any,
  update?: Function;
  delete?: Function;
  updateField?: Function;
  deleteField?: Function;
  action?: string;
}

interface CardState {
  value: any;
  config?: any;
  update?: Function;
  delete?: Function;
  updateField?: Function;
  deleteField?: Function;
  action?: string;
  fieldBreadcrumb?: any;
  showCopyToClipboardTooltip?:boolean;
  mouseOn?: boolean;
}

export default injectIntl(Card);
