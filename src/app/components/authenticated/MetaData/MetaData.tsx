import React, { Component } from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import { Breadcrumb, Row, Container, Col, InputGroup, Form, Tooltip, Overlay, OverlayTrigger } from 'react-bootstrap';
//import * as  QRCode from 'qrcode.react';
const QRCode = require('qrcode.react');
import Field from '../Field/Field';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


class MetaData extends Component<MetaDataProps, MetaDataState> {
  
  cardIdInput?: any;
  qrCode?: any;
  copyToClipboardButton?: any;

	constructor(props: MetaDataProps) {
    super(props);
    this.state = this.defaultState();
	}

  defaultState(){
    const { value } = this.props;
    return { value };
  }

  render(){
    return (<div></div>);
  }

  renderEdit(){

  }

  renderShow(){}

}

interface MetaDataProps extends InjectedIntlProps{
  value: any;
}

interface MetaDataState {
  value: any;
}

export default injectIntl(MetaData);
