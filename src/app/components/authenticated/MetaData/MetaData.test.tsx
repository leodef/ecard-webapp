import React from 'react';
import ReactDOM from 'react-dom';
import MetaData from './MetaData';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MetaData {...{value:{}, config: {}}}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
