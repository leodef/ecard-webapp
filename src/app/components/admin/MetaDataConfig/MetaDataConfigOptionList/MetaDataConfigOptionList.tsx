import React, { Component } from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import MetaDataConfig from '../MetaDataConfig';
import MetaDataConfigOption from '../MetaDataConfigOption/MetaDataConfigOption';

class MetaDataConfigOptionList extends Component<MetaDataConfigProps, MetaDataConfigState> {

	constructor(props: MetaDataConfigProps) {
    super(props);
    this.state = this.defaultState();
	}

  defaultState(){
    const props = this.props;
    let { value, action, config, update } = props
    const deleteF = props.delete
    action = action || 'SHOW'
    return { 
      value,
      action,
      config,
      update,
      delete: deleteF
    };
  }

  render(){
    const { value, action, config, update } = this.state;
    const deleteF = this.state.delete;
    return value.map( (val: any) => {
      const params = {
        value: val,
        action,
        config,
        update,
        delete: deleteF};
      return (<MetaDataConfigOption {...params}></MetaDataConfigOption>)
    });
  }
}

interface MetaDataConfigProps extends InjectedIntlProps{
  action?: string;
  config: any;
  value: Array<any>;
  update?: Function;
  delete?: Function;
}

interface MetaDataConfigState {
  action: string;
  config: any;
  value: Array<any>;
  update?: Function;
  delete?: Function;
}

export default injectIntl(MetaDataConfigOptionList);
