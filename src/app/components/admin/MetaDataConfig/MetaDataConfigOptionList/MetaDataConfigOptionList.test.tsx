import React from 'react';
import ReactDOM from 'react-dom';
import MetaDataConfigOptionList from './MetaDataConfigOptionList';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const params = {value: new Array<any>(), config: {}, action: 'SHOW'};
  ReactDOM.render(<MetaDataConfigOptionList {...params} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
