import React from 'react';
import ReactDOM from 'react-dom';
import MetaDataConfigList from './MetaDataConfigList';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const params = {value: new Array<any>(), action: 'SHOW'};
  ReactDOM.render(<MetaDataConfigList {...params} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
