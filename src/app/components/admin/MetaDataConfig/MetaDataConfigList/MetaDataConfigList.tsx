import React, { Component } from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import MetaDataConfig from '../MetaDataConfig';

class MetaDataConfigList extends Component<MetaDataConfigProps, MetaDataConfigState> {

	constructor(props: MetaDataConfigProps) {
    super(props);
    this.state = this.defaultState();
	}

  defaultState(){
    const props = this.props;
    let { value, action, config, update, updateOption, deleteOption } = props
    const deleteF = props.delete
    action = action || 'SHOW'
    return { 
      value,
      action,
      config, 
      update,
      delete: deleteF,
      updateOption,
      deleteOption
    };
  }

  render(){
    const {
      value,
      action,
      config,
      update,
      updateOption,
      deleteOption
    } = this.state;
    const deleteF = this.state.delete
    return value.map( (val: any) => {
      const params = {
        value: val,
        action,
        config,
        update,
        delete: deleteF,
        updateOption,
        deleteOption
      };
      return (<MetaDataConfig {...params}></MetaDataConfig>)
    });
  }
}

interface MetaDataConfigProps extends InjectedIntlProps{
  action?: string;
  config: any;
  value: Array<any>;
  update?: Function;
  delete?: Function;
  updateOption?: Function;
  deleteOption?: Function;
}

interface MetaDataConfigState {
  action: string;
  config: any;
  value: Array<any>;
  update?: Function;
  delete?: Function;
  updateOption?: Function;
  deleteOption?: Function;
}

export default injectIntl(MetaDataConfigList);
