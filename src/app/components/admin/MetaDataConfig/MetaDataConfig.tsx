import React, { Component } from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import { Card, Row, Col, Container, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import MetaDataConfigOptionList from './MetaDataConfigOptionList/MetaDataConfigOptionList';

class MetaDataConfig extends Component<MetaDataConfigProps, MetaDataConfigState> {

	constructor(props: MetaDataConfigProps) {
    super(props);
    this.state = this.defaultState();
	}

  defaultState(): MetaDataConfigState{
    let {
      value,
      action,
      config,
      update,
      updateOption,
      deleteOption  } = this.props;

    const deleteF = this.props.delete;
    action = action || 'SHOW';
    return { 
      value,
      action,
      config,
      update,
      delete: deleteF,
      updateOption,
      deleteOption,
    };
  }

  edit(){
    this.setState({action: 'EDIT'}); 
  }

  show(){
    this.setState({action: 'SHOW'}); 
  }

  update(){
    const { value, update } = this.state;
    if(update){
      update(value);
    }
  }
  
  delete(){
    const { value } = this.state;
    const deleteF = this.state.delete;
    if(deleteF){
      deleteF(value);
    }
  }

  updateOption(){
    const { value, updateOption } = this.state;
    if(updateOption){
      updateOption(value);
    }
  }

  deleteOption(){
    const { value, deleteOption } = this.state;
    if(deleteOption){
      deleteOption(value);
    }
  }

  onPointerEnter(event: any){
    this.setState({mouseOn: true});
  }

  onPointerLeave(event: any){
    this.setState({mouseOn: false});
  }

  render(){
    const { action } = this.state;
    switch(action){
      case 'EDIT':
        return this.renderEdit();
      case 'SHOW':
        return this.renderShow();
      default:
        return this.renderShow();
    }
  }

  updateValue(obj: any){
    this.setState(state => ({
      value: {
        ...state.value,
        ...obj
      }
    }));
  }


  // render
  renderEdit(){
		const { value } = this.props;
    const { config, mouseOn } = this.state;
    const permissions = config.metaData.permissions;
    if((
      permissions &&
      !permissions.edit
      )){return (<span>Unauthorized</span>);}
    return (
      <Card>
        onPointerEnter={this.onPointerEnter.bind(this)}
        onPointerLeave={this.onPointerLeave.bind(this)}>
        <Card.Body as={Form}>
          <Card.Title as={Row}>
            <Col></Col>
                <Form.Group as={Col} controlId="title">
                <Form.Label>
                  Title
                </Form.Label>
                <Form.Control
                  type="text"
                  name="title"
                  placeholder="Title"
                  defaultValue={value.title}
                  onChange={(val: any)=> this.updateValue({title: val})}
                  />
                </Form.Group>
            <Col>
            { !mouseOn ? null: (<Row>
              { !permissions.edit ? null :
                (<span onClick={this.update.bind(this)}>
                  <FontAwesomeIcon
                    icon="save"
                    className="align-top ml-2" />
                </span>)
              }
              { !permissions.show ? null :
                (<span onClick={this.show.bind(this)}>
                  <FontAwesomeIcon
                    icon="times"
                    className="align-top ml-2" />
                </span>)
              }
              { !permissions.delete ? null:
                  (<span onClick={this.delete.bind(this)}>
                    <FontAwesomeIcon
                      icon="trash"
                      className="align-top ml-2" />
                  </span>)
              }
              </Row>) }
            </Col>
          </Card.Title>
          <Card.Subtitle as={Row} className="mb-2 text-muted">
            <Form.Group as={Col} controlId="key">
                <Form.Label>
                  Key
                </Form.Label>
                <Form.Control
                  type="text"
                  name="key"
                  placeholder="Key"
                  defaultValue={value.title}
                  onChange={(val: any)=> this.updateValue({key: val})}
                  />
                </Form.Group>
          </Card.Subtitle>
          <Form.Group as={Row} controlId="type">
                <Form.Control
                  as={Col}
                  type="text"
                  name="type"
                  placeholder="Type"
                  defaultValue={value.type}
                  onChange={(val: any)=> this.updateValue({type: val})}
                  />
                  <Form.Control
                  as={Col}
                  type="text"
                  name="key"
                  placeholder="Key"
                  defaultValue={value.title}
                  onChange={(val: any)=> this.updateValue({key: val})}
                  />
                </Form.Group>
          <p>{value.type} - {value.required?'Required': 'Not Required'}</p>
          {
            this.renderOption()
          }
        </Card.Body>
      </Card>);
  }

  renderShow() {
    const { value, config, mouseOn } = this.state;
    const permissions = config.metaData.permissions;
    if((
      permissions &&
      !permissions.show
      )){return (<span>Unauthorized</span>);}
    return (
		  <Card
        onPointerEnter={this.onPointerEnter.bind(this)}
        onPointerLeave={this.onPointerLeave.bind(this)}>
        <Card.Body>
          <Card.Title as={Row}>
            <Col></Col>
            <Col>
              {value.title}
            </Col>
            <Col>
            { !mouseOn ? null: (<Row>
                { !permissions.edit ? null :
                  (<span onClick={this.edit.bind(this)}>
                    <FontAwesomeIcon
                      icon="edit"
                      className="align-top ml-2" />
                  </span>)
                }
                { !permissions.delete ? null:
                    (<span onClick={this.delete.bind(this)}>
                      <FontAwesomeIcon
                        icon="trash"
                        className="align-top ml-2" />
                    </span>)
                }
              </Row>) }
            </Col>
          </Card.Title>
          <Card.Subtitle className="mb-2 text-muted">
            {value.key}
          </Card.Subtitle>
          <p>{value.type} - {value.required?'Required': 'Not Required'}</p>
          {
            this.renderOption()
          }
        </Card.Body>
      </Card>
    );
  }

  renderOption(){
    const {value, config} = this.state
    const params = {
      value,
      config,
      update: this.updateOption.bind(this),
      delete: this.deleteOption.bind(this)
    }
    return (
      <Container>
      <Row className="justify-content-md-center">
        <Col>
        <MetaDataConfigOptionList {...params}/>
        </Col>
      </Row>
      </Container>);
  }

}
interface MetaDataConfigProps extends InjectedIntlProps{
  action?: string;
  value: any;
  config: any;
  update?: Function;
  delete?: Function;
  updateOption?: Function;
  deleteOption?: Function;
}

interface MetaDataConfigState {
  action: string;
  value: any;
  config: any;
  update?: Function;
  delete?: Function;
  updateOption?: Function;
  deleteOption?: Function;
  mouseOn?: boolean;
}

export default injectIntl(MetaDataConfig);
