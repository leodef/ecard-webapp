import React from 'react';
import ReactDOM from 'react-dom';
import MetaDataConfig from './MetaDataConfig';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const params = {value: new Array<any>(), config: {}, action: 'SHOW'};
  ReactDOM.render(<MetaDataConfig {...params} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
