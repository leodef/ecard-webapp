import React, { Component } from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import { Container, Row, Col, Form, InputGroup } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class MetaDataConfigOption extends Component<MetaDataConfigProps, MetaDataConfigState> {

	constructor(props: MetaDataConfigProps) {
    super(props);
    this.state = this.defaultState();
	}

  defaultState(){
    const props = this.props;
    let { value, action, config, update } = props
    const deleteF = props.delete
    action = action || 'SHOW'
    return { 
      value,
      action,
      config, 
      update,
      delete: deleteF
    };
  }

  edit(){
    this.setState({action: 'EDIT'}); 
  }

  show(){
    this.setState({action: 'SHOW'}); 
  }
  
  delete(){
    const { value } = this.state;
    const deleteF = this.state.delete;
    if(deleteF){
      deleteF(value);
    }
  }

  update(){
    const { update, value } = this.state;
    if(update){
      update(value);
    }
  }

  onPointerEnter(event: any){
    this.setState({mouseOn: true});
  }

  onPointerLeave(event: any){
    this.setState({mouseOn: false});
  }

  render() {
    const {  action } = this.state;
    switch(action){
      case 'EDIT':
        return this.renderEdit();
      case 'SHOW':
        return this.renderShow();
      default:
          return this.renderShow();
      }
  }

  updateValue(obj: any){
    this.setState(state => ({
      value: {
        ...state.value,
        ...obj
      }
    }));
  }

  renderEdit(){
    const { value, config, mouseOn} = this.state;
    const permissions = config.metaDataOption.permissions;
    if((
      permissions &&
      !permissions.edit
      )){return (<span>Unauthorized</span>);}
    return (
      <Container
      as={Container}
      onPointerEnter={this.onPointerEnter.bind(this)}
      onPointerLeave={this.onPointerLeave.bind(this)}>
      <Form as={Row} >
        <InputGroup as={Col} className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text>Option</InputGroup.Text>
          </InputGroup.Prepend>
          <Form.Control
            type="text"
            name="title"
            placeholder="title"
            onChange={(val: any)=> this.updateValue({title: val})}
            defaultValue={value.title}
            />
            <Form.Control
            type="text"
            name="value"
            placeholder="value"
            onChange={(val: any)=> this.updateValue({value: val})}
            defaultValue={value.value}
            />
        </InputGroup>
      <Col sm={1}>
      { !mouseOn? null: (<Row>
        {!permissions.show ? null : <Col>
          <span onClick={this.show.bind(this)}>
            <FontAwesomeIcon
              icon="times"
              className="align-top ml-2" />
          </span> </Col>
        }
        { !permissions.edit ? null : <Col>
          <span onClick={this.update.bind(this)}>
            <FontAwesomeIcon
              icon="save"
              className="align-top ml-2" />
          </span> </Col>
        }
        { !permissions.delete ? null:
          (<span onClick={this.delete.bind(this)}>
            <FontAwesomeIcon
              icon="trash"
              className="align-top ml-2" />
          </span>)
        } </Row>)}
      </Col>
      </Form>
    </Container>);
  }

  renderShow(){
    const { value, config, mouseOn } = this.state;
    const permissions = config.metaDataOption.permissions;
    if((
      permissions &&
      !permissions.show
      )){return (<span>Unauthorized</span>);}
    return (
    <Container
      onPointerEnter={this.onPointerEnter.bind(this)}
      onPointerLeave={this.onPointerLeave.bind(this)}>
      <Row>
        <Col>
          <span>
            <strong className="mr-2">
              {value.title}
            </strong>
            {value.value}
          </span>
        </Col>
        <Col sm={1}>
        { !mouseOn?  null: (<Row>
            {!permissions.edit ? null :
              (<span onClick={this.edit.bind(this)}>
                <FontAwesomeIcon
                  icon="edit"
                  className="align-top ml-2" />
              </span>)
            }
            { !permissions.delete ? null:
              (<span onClick={this.delete.bind(this)}>
                <FontAwesomeIcon
                  icon="trash"
                  className="align-top ml-2" />
              </span>)
            }
            </Row>)}
        </Col>
        }
      </Row>
    </Container>);
  }
}

interface MetaDataConfigProps extends InjectedIntlProps{
  action?: string;
  value: any;
  config: any;
  update?: Function;
  delete?: Function;
}

interface MetaDataConfigState {
  action: string;
  value: any;
  config: any;
  update?: Function;
  delete?: Function;
  mouseOn?: boolean;
}

export default injectIntl(MetaDataConfigOption);
