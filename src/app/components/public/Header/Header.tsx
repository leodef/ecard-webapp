import React, { Component } from 'react';
import { FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { LinkContainer } from 'react-router-bootstrap';
import {Navbar, Nav} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


class Header extends Component<HeaderProps, {}> {
	constructor(props: HeaderProps) {
    super(props);
	}


  render() {
		const { intl } = this.props;
    return (
			<Navbar bg="primary" variant="dark">
				<LinkContainer to="/">
  		  <Navbar.Brand>
					<FontAwesomeIcon size="lg" icon="address-card" className="d-inline-block align-top ml-2" />
  		    {intl.formatMessage({ id: 'public.Header.title' })}
  		  </Navbar.Brand>
				</LinkContainer>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
  		<Navbar.Collapse id="basic-navbar-nav">
  		  <Nav className="ml-auto">
				<LinkContainer to="/login">
					<Nav.Link>
          	<FormattedMessage id="public.Header.login" defaultMessage="Login"/>
					</Nav.Link>
				</LinkContainer>
				<LinkContainer to="/signup">
      		<Nav.Link>
          	<FormattedMessage id="public.Header.signup" defaultMessage="Signup"/>
					</Nav.Link>
				</LinkContainer>

				
				<LinkContainer to="/contacts">
      		<Nav.Link>
          	Contacts
					</Nav.Link>
				</LinkContainer>
				<LinkContainer to="/metadataconfig">
      		<Nav.Link>
          	MetaData Config
					</Nav.Link>
				</LinkContainer>

			</Nav>
		</Navbar.Collapse>
  </Navbar>
	 
    );
  }
}
interface HeaderProps extends InjectedIntlProps{

}

export default injectIntl(Header);
